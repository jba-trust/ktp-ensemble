# coding: utf8

import re
import os
import sys
import logging
import contextlib
import geocoding
import dateparser
import spacy
import pdf_to_text
from typing import Optional, Dict
from pathlib import Path
from datetime import datetime
from open_struct import OpenStruct
from output_type import OutputType


# TODO:
#  Look at linking entities, to create a single flood event with all the info about it.
#  https://spacy.io/usage/training#intent-parser
spacy.prefer_gpu()

logging.basicConfig(format="[%(name)s] %(levelname)s: %(message)s")
logger = logging.getLogger("Flood Extraction")

config = OpenStruct()
config.model_directory = None
# TODO: Remove provenance_filepath value (set to None), and instead allow as CLI argument
config.provenance_filepath = None#Path(r"C:\Users\huwtaylor\Development\ktp-ensemble\nlp_flood\provenance.log")
config.entities = set()
config.default_output_format = "{}"
config.entity_formats = dict()
config.date_settings = {
    'PREFER_DATES_FROM': 'past',
    'PREFER_DAY_OF_MONTH': 'first',
}
config.country_hint = 'uk'


def empty_data_point() -> Dict[str, Optional[any]]:
    return dict((entity, None) for entity in config.entities)


def add_provenance(text: str, label: str, value: any = None):
    if config.provenance_filepath is None:
        return
    # noinspection PyTypeChecker
    with open(config.provenance_filepath, 'a', encoding="utf-8") as file:
        if value is not None:
            formatted_value = (config.entity_formats.get(label, config.default_output_format)).format(value)
            message = f"[{label}] \"{text}\" => {formatted_value}"
        else:
            message = f"[{label}] {text}"
        file.write(message + "\n")

#TODO: Work out why the text is processed as a huge block
class TextChunk:

    def __init__(self, analysis: "Analysis", parent: "TextChunk", text: str,
                 title: str = "", summary_datapoint: dict = None):
        self.analysis = analysis
        self.parent = parent
        self.text = text
        self.title = title
        self.knowledge_stack = {}
        self.datapoints = []
        self.summary_data = summary_datapoint  # This is from a title, or overview of a section
        self.current_datapoint = empty_data_point()

    def get_knowledge(self, key: str):
        if self.parent is None:
            return [self.knowledge_stack.get(key)]
        else:
            return self.parent.get_knowledge(key) + [self.knowledge_stack.get(key)]

    def extract_data(self):
        self.generate_datapoints(self.text, self.summary_data)
        return self.datapoints

    def add_to_datapoint(self, label: str, text: str):
        if label == "LOCATION":
            self.knowledge_stack["LOCATION"] = text
            location_text = ",".join([x for x in self.get_knowledge("LOCATION")[::-1] if x is not None])
            lat_long = geocoding.get_latlong(location_text)
            address = geocoding.get_address(location_text)
            if lat_long is not None and address is not None:
                add_provenance(location_text, label, lat_long)
                self.current_datapoint[label] = (lat_long, address)
            elif lat_long is not None:
                add_provenance(location_text, label, lat_long)
                self.current_datapoint[label] = (lat_long, None)
            elif address is not None:
                add_provenance(location_text, "ADDRESS", address)
                self.current_datapoint["ADDRESS"] = (None, address)
        elif label == "DATE":
            self.knowledge_stack["DATE"] = text
            date_text = text
            date = dateparser.parse(date_text, settings=config.date_settings)
            if date:
                self.current_datapoint[label] = date
        else:
            self.current_datapoint[label] = text

        # if all(self.current_datapoint[entity] is not None for entity in self.current_datapoint):
        if True:
            self.datapoints.append(self.current_datapoint)
            self.current_datapoint = empty_data_point()  # TODO: Allow for setting to default_datapoint

            # TODO:
            #  How should incomplete datapoints be handled? If this uses the parent's knowledge, then potentially all
            #  labels will yield datapoints.
            #  Maybe only certain labels should search for parent's knowledge.

            # TODO:
            #  If finding an entity that fills a datapoint resets than entity to nothing, but keeps the others, then
            #  more general entities will persist and be referred to by lots of more specific entities.
            #  For example, if a location is found and a date has been previously mentioned, then the location field is
            #  cleared, and the date persists to be referred to by the next location. If it alternates date, location,
            #  date, then perhaps it should reference the last datapoint for any missing values?

            # TODO:
            #  I think the next big job is to work on splitting the text up. Document -> Chapters -> Sections ->
            #  Paragraphs -> Sentences. And get the knowledge hierarchy working.

    def generate_datapoints(self, text: str, default_datapoint: Optional[dict] = None):
        if default_datapoint is None:
            self.current_datapoint = empty_data_point()
        else:
            self.current_datapoint = default_datapoint
        document = self.analysis.model(text)
        for sentence_span in document.sents:
            sentence_text = re.sub("\n", " ", sentence_span.text)
            parsed_sentence = self.analysis.model(sentence_text)
            for entity in parsed_sentence.ents:
                text, label = str(entity.text), str(entity.label_)
                logger.debug("\t{}: '{}'".format(label, text))
                if label not in config.entities:
                    logger.warning("Found unexpected label '{}'. Ignoring.".format(label))
                    continue
                else:
                    self.add_to_datapoint(label, text)


class TextSection(TextChunk):

    def __init__(self, analysis: "Analysis", parent: Optional["TextChunk"], text: str, title: str = "",
                 datapoint: Optional[dict] = None):
        if datapoint is None:
            datapoint = empty_data_point()
        super().__init__(analysis, parent, text, title, datapoint)
        self.child_texts = []
        self.knowledge_stack = {}
        self.find_child_texts()

    def find_child_texts(self):
        # TODO: split text up into chunks somehow
        self.child_texts.append(TextChunk(self.analysis, self, self.text))

    def extract_data(self):
        datapoints = []
        logger.debug("Extracting data from {} text chunks".format(len(self.child_texts)))
        for text in self.child_texts:
            datapoints.extend(text.extract_data())
        return datapoints


class Document:

    def __init__(self, analysis: "Analysis", filename: Path, title: str = ""):
        self.analysis = analysis
        self.filename = filename
        self.contents = self.get_text_contents()
        self.text = None
        if self.contents is not None:
            self.text = TextSection(self.analysis, None, self.contents, title)

    def get_text_contents(self):
        file_extension = self.filename.suffix
        # TODO: Add other file-type conversions
        if file_extension.lower() == ".pdf":
            return pdf_to_text.convert_pdf(self.filename)
        elif file_extension.lower() == ".txt":
            # noinspection PyTypeChecker
            with open(self.filename, 'r', encoding="utf-8") as input_file:
                return input_file.read()
        else:
            logger.warning(f"Encountered unexpected file type '{file_extension}'.")
            return ""

    def find_data(self):
        if self.text is not None:
            return self.text.extract_data()
        logger.warning("document.text was None")
        return []


class Analysis:

    def __init__(self, source_paths: list):
        # TODO: use glob.glob here?
        directories = [d for d in source_paths if os.path.isdir(d)]
        files = [f for f in source_paths if os.path.isfile(f)]
        files += [Path(d, f) for d in directories for f in os.listdir(d) if os.path.isfile(os.path.join(d, f))]
        self.datapoints = []
        self.model = None
        self.source_filenames = files
        self.load_model()

    def load_model(self):
        self.model = spacy.load(config.model_directory)
        logger.info("NLP Model loaded.")

    def run(self):
        logger.info("Analysing %d files." % len(self.source_filenames))
        try:
            geocoding.load_cache()
            # with ThreadPoolExecutor() as executor:
            #     for document in executor.map(Document, self.source_filenames):
            #         self.datapoints += document.find_data()
            for filename in self.source_filenames:
                logger.debug("Beginning analysis of %s" % filename)
                document = Document(self, filename, os.path.basename(filename))
                self.datapoints += document.find_data()
        finally:
            geocoding.save_cache()
        logger.info("Analysis complete.")

    @contextlib.contextmanager
    def open_output_file(self, filepath: Optional[Path], mode: str = 'w'):
        if filepath:
            # noinspection PyTypeChecker
            fh = open(filepath, mode, encoding='utf-8')
        else:
            fh = sys.stdout
        try:
            yield fh
        finally:
            if fh is not sys.stdout:
                fh.close()

    @staticmethod
    def format_datapoint(datapoint: dict, separator: str = ",") -> str:
        formatted_values = [(config.entity_formats.get(x, config.default_output_format)).format(datapoint[x])
                            for x in config.entities]
        output = separator.join(formatted_values)
        return output

    def output_results(self, output_type: OutputType, output_location: Optional[Path] = None) -> list:
        if output_type == OutputType.NONE:
            pass  # Don't output anything (but still return results)
        elif output_type == OutputType.HTML:
            if output_location is None:
                error_message = "Output type HTML was specified, but no path was provided."
                logger.error(error_message)
                raise Exception(error_message)
            self.output_to_html(output_location)
        elif output_type == OutputType.CSV:
            if output_location is None:
                error_message = "Output type CSV was specified, but no path was provided."
                logger.error(error_message)
                raise Exception(error_message)
            self.output_to_csv(output_location)
        else:
            error_message = "Invalid output type '{}'.".format(output_type.name)
            logger.error(error_message)
            raise Exception(error_message)
        return self.datapoints

    def output_to_csv(self, filepath: Optional[Path]):
        logger.debug("Outputting {} results to CSV...".format(len(self.datapoints)))
        # TODO: Add a header row.
        with self.open_output_file(filepath) as output_file:
            for datapoint in self.datapoints:
                output_file.write(Analysis.format_datapoint(datapoint, ",") + "\n")

    def output_to_html(self, filepath: Path):
        pass  # TODO: use Displacy


def analyse(model_path: Path, entities: set, output_formats: Optional[dict], *paths: Path) -> Optional["Analysis"]:
    config.model_directory = model_path
    geocoding.country_hint = config.country_hint
    if output_formats is not None:
        config.entity_formats = output_formats
    if entities is not None:
        config.entities = entities
    """Extract flood events from the given input filepaths"""
    if paths:
        analysis = Analysis(list(paths))
        add_provenance(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"), "BEGIN ANALYSIS")
        analysis.run()
        add_provenance(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"), "END ANALYSIS")
        return analysis
    else:
        logger.warning("No paths provided.")
        return None


def main(model: Path, paths: list, labels: list, output_path: Optional[Path], output_type: OutputType, 
         country_hint: str, provenance_filepath: Path, logging_level=logging.INFO):
    """Extract flood data from naturally-written documents"""
    logger.setLevel(logging_level)

    if country_hint is not None:
        config.country_hint = country_hint
    if provenance_filepath is not None:
        config.provenance_filepath = provenance_filepath
    analysis = analyse(model, set(labels), None, *paths)
    analysis.output_results(output_type, output_path)
