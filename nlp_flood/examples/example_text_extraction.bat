@set wd=%cd%
@cd %~dp0

@..\env\Scripts\python.exe -u ..\extract.py ^
 "..\model" ^
 "C:\Users\huwtaylor\Development\ktp-ensemble\nlp_flood\examples\texts\green-street-chorleywood-section-19-flood-investigation-final-report.pdf" ^
 --label "SOURCE" ^
 --label "CAUSE" ^
 --label "LOCATION" ^
 --label "DATE" ^
 --output-type csv ^
 --output-path "outputs\extraction.csv" ^
 --debug

@pause
@cd %wd%
