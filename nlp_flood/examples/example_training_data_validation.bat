@set wd=%cd%
@cd %~dp0

@..\env\Scripts\python.exe -u ..\train.py ^
 "..\model" ^
 --training-data "training_data\filled_training_*" ^
 --validate

@pause
@cd %wd%
