# coding: utf8

# All entities:
#   - LOCATION
#   - DATE
#   - SOURCE
#   - EFFECT
#   - WATERWAY
#   - CAUSE
#   - LLFA

# Training Data for Flood_Investigation_Report_Newmarket
# \\SKI-STORAGE04\LiveData\2019\Projects\2019s1202 - Environment Agency - South West Region - RFO CA Stage 2\2_Shared\_Incoming\Section 19 Reports\EAN\Cambridgeshire County\Flood_Investigation_Report_Newmarket.pdf
[
    {
        "text": 'Flood Investigation Report',
        "entities": [
        
        ]
    },
    {
        "text": 'Flood Investigation Report   Newmarket 1.1 Background As the Lead Local Flood Authority (LLFA) for Cambridgeshire, it is Cambridgeshire County Council’s (CCC) duty to investigate flood incidents as detailed within Section 19 of the Flood and Water Management Act 2010.  ',
        "entities": [
            ("LOCATION", "Newmarket"),
            ("LLFA", "Cambridgeshire County Council"),
        ]
    },
    {
        "text": 'CCC carries out flood investigation, in order to comply with current legislation. Unfortunately owing to very limited resources we cannot investigate every incident, and as such we have prioritised investigations.   1.2 Risk Management Authority  The flood investigation reports have been produced in partnership with the relevant Risk Management Authorities (RMA). RMAs are defined by the Flood and Water Management Act 2010, as they have responsibilities for flood risk management.  The RMAs have a duty to carry out flood risk management functions in a manner consistent with national and local strategies.   The RMAs in Cambridgeshire are listed below; Table 1 highlights the relevant RMA that will take the lead in managing the risk from various local sources of flooding.  ',
        "entities": [
        
        ]
    },
    {
        "text": ' Anglian Water (AW)   City and District Councils (C&DC)  Environment Agency (EA)    Highway Authority (HA) part of CCC  Internal Drainage Board (IDB)   ',
        "entities": [
        
        ]
    },
    {
        "text": 'The flood investigation reports determine which RMA(s) have exercised their flood risk management functions in response to flooding.   Table 1 Relevant flood Risk Management Authorities that will take the lead in managing the risk from various local sources of flooding. Flood and Water Management Act 2010: Section 19 – Local Authorities: Investigations ',
        "entities": [
        
        ]
    },
    {
        "text": '1) On becoming aware of a flood in its area, a lead local flood authority must, to the    extent that it considers it necessary or appropriate, investigate - a) which risk management authorities have relevant flood risk management functions, and b) whether each of those risk management authorities has exercised, or is proposing to exercise, those functions in response to the flood. 2) Where an authority carries out an investigation under subsection (1) it must - a) publish the results of its investigation, and b) notify any relevant risk management authorities. ',
        "entities": [
        
        ]
    },
    {
        "text": 'Flood Sources EA LLFA C&DC  AW HA IDBs RIVERS       Main River        Ordinary Watercourse       Awarded Watercourse       Ground Water       SURFACE RUNOFF       Surface water       Surface water originating on the highway       OTHER       Sewer flooding        The Sea, Reservoirs       ',
        "entities": [
        
        ]
    },
    {
        "text": 'Owners of land with a watercourse running through it are termed as ‘riparian owners’ and often have responsibilities to maintain and allow the free flow of water through their land. Further information on riparian owner rights can be found on the CCC website.  Flood Incident, Extent and Impact: Table 2. Formal Flood Investigation Report Date of Incident August 2014  Reference Number  FI/14/000090 Location of Incident  Crocksford Road, Newmarket  Risk Management Authority Reported to CCC Incident Reported by Resident ',
        "entities": [
            ("DATE", "August 2014"),
            ("LOCATION", "Crocksford Road, Newmarket"),
        ]
    },
    {
        "text": 'Figure 1. Location of flood incident  Note: The above map is indicative and does not represent the true extent of flooding. Historical Flooding Unknown Source of Flooding Groundwater flooding  Depth Unknown  http://www.cambridgeshire.gov.uk/info/20099/planning_and_development/49/water_minerals_and_waste/4http://www.cambridgeshire.gov.uk/info/20099/planning_and_development/49/water_minerals_and_waste/4',
        "entities": [
            ("CAUSE", "Groundwater flooding"),
        ]
    },
    {
        "text": 'Likely Cause of Incident  The cause of flooding is unclear. Flooding may have occurred as a result of a heavy rainfall event, this may have caused the water to enter the property through the walls of the basement. It is believed to be a result of ground water flooding, there were no evidence of other flood sources. Summary of Impact 1 property was flooded internally Action Taken by Relevant Risk Management Authorities HA The department had ordered contractors to map and clean the system plus find the potential outfall if possible.  Investigation highlighted that flooding was not a result of highway flooding as the properties are considerably higher than the road and the road itself did not flood.  Further solutions CCC recommends that residents, whose properties have been flooded contact the National Flood Forum for details on how to protect their property. ',
        "entities": [
            ("CAUSE", "The cause of flooding is unclear"),
        ]
    },
    {
        "text": 'Please Note: The actions undertaken, as part of this flood investigation does not preclude the risk of the area flooding in the future. If you live in an area that is at a risk of flooding, CCC would encourage you undertake the action highlighted in the ‘Further Solutions’ section in Table 2. Next Steps The next steps for the LLFA is to ensure the report is forwarded to the RMAs identified in the above table and that these authorities are aware of their flood risk management responsibilities. ',
        "entities": [
        
        ]
    },
    {
        "text": 'http://www.nationalfloodforum.org.uk/',
        "entities": [
        
        ]
    },
]
