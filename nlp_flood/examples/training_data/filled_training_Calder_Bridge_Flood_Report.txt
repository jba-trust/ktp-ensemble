# coding: utf8

# All entities:
#   - LOCATION
#   - DATE
#   - SOURCE
#   - EFFECT
#   - WATERWAY
#   - CAUSE
#   - LLFA

# Training Data for Calder Bridge Flood Report
# \\SKI-STORAGE04\LiveData\2019\Projects\2019s1202 - Environment Agency - South West Region - RFO CA Stage 2\2_Shared\_Incoming\Section 19 Reports\CLA\Cumbria County\Calder Bridge Flood Report.pdf
[
    {
        "text": 'Calder Bridge Flood Report',
        "entities": [
            ("LOCATION", "Calder Bridge"),
        ]
    },
    {
        "text": 'Calder Bridge ',
        "entities": [
            ("LOCATION", "Calder Bridge"),
        ]
    },
    {
        "text": 'Flood Investigation Report No. 52 ',
        "entities": [
        
        ]
    },
    {
        "text": 'Flood Events 2012 ',
        "entities": [
            ("DATE", "2012"),
        ]
    },
    {
        "text": ' Cumbria County Council ',
        "entities": [
        
        ]
    },
    {
        "text": '2     Serving the people of Cumbria This flood investigation report has been produced by Cumbria County Council as a Lead Local Flood Authority under Section 19 of the Flood and Water Management Act 2010. ',
        "entities": [
            ("LLFA", "Cumbria County Council"),
        ]
    },
    {
        "text": 'Version ',
        "entities": [
        
        ]
    },
    {
        "text": 'Undertaken by ',
        "entities": [
        
        ]
    },
    {
        "text": 'Reviewed by ',
        "entities": [
        
        ]
    },
    {
        "text": 'Approved by ',
        "entities": [
        
        ]
    },
    {
        "text": 'Date ',
        "entities": [
        
        ]
    },
    {
        "text": 'Preliminary ',
        "entities": [
        
        ]
    },
    {
        "text": 'David White ',
        "entities": [
        
        ]
    },
    {
        "text": 'Anthony Lane ',
        "entities": [
        
        ]
    },
    {
        "text": '18th Oct 2013 ',
        "entities": [
        
        ]
    },
    {
        "text": 'Draft ',
        "entities": [
        
        ]
    },
    {
        "text": 'Andrew Harrison ',
        "entities": [
        
        ]
    },
    {
        "text": '   Anthony Lane ',
        "entities": [
        
        ]
    },
    {
        "text": '    16th Dec 2013  ',
        "entities": [
        
        ]
    },
    {
        "text": 'Published ',
        "entities": [
        
        ]
    },
    {
        "text": 'Andrew Harrison ',
        "entities": [
        
        ]
    },
    {
        "text": 'Anthony Lane ',
        "entities": [
        
        ]
    },
    {
        "text": 'Doug Coyle ',
        "entities": [
        
        ]
    },
    {
        "text": ' 31st March 2014 ',
        "entities": [
        
        ]
    },
    {
        "text": 'cumbria.gov.uk     3 Contents Executive Summary ............................................................................................................................................. 4 Event Background ............................................................................................................................................... 5 Flooding Incident ................................................................................................................................................... 5 Investigation ........................................................................................................................................................ 7 Rainfall Event…………………………………………………………………………………………………………………….7 Map of Flow Routes ............................................................................................................................................... 9 Likely Causes of Flooding .................................................................................................................................... 11 Subsequent Remedial Work................................................................................................................................  11 Flooding History ................................................................................................................................................... 12 Recommended Actions ..................................................................................................................................... 13 Next Steps .......................................................................................................................................................... 14 Appendices ........................................................................................................................................................ 15 Appendix 1: Glossary ........................................................................................................................................... 15 Appendix 2: Summary of Relevant Legislation and Flood Risk Management Authorities ....................................... 16 Appendix 3: Useful contacts and links .................................................................................................................. 19 ',
        "entities": [
        
        ]
    },
    {
        "text": 'Figures Figure 1.  Location Plan. ........................................................................................................................................ 5 Figure 2.  Calder Bridge Surrounding Area Plan ..................................................................................................... 6 Figure 3.  Calder Bridge Village Plan ...................................................................................................................... 6 Figure 4.  Rainfall Data .......................................................................................................................................... 7 Figure 5.  Radar Picture of Rainfall at Calder Bridge at 1am on 30th August 2012 .................................................. 8 Figure 6.  Calder Bridge Flood Routes Overview .................................................................................................... 9 Figure 7.  Calder Bridge Flood Routes and Flood Extent ...................................................................................... 10 Figure 8.  Site of Calder Bridge Highway Drainage Outfall .................................................................................... 12 ',
        "entities": [
        
        ]
    },
    {
        "text": ' Cumbria County Council ',
        "entities": [
        
        ]
    },
    {
        "text": '4     Serving the people of Cumbria Executive Summary ',
        "entities": [
        
        ]
    },
    {
        "text": 'Cumbria County Council as Lead Local Flood Authority has prepared this report with the assistance of other Flood Risk Management Authorities as it considers necessary to do so under Section 19 of the Flood and Water Management Act 2010. ',
        "entities": [
            ("LLFA", "Cumbria County Council"),
        ]
    },
    {
        "text": 'The excessive storm event of the 30th August 2012, with intensive rainfall on already saturated fields, caused flooding to the highway and to properties in Calder Bridge.  Cumbria County Council Area Highways Team have been identifying the sources of the runoff causing the flooding and have carried out remedial work to deal with the consequences.   It is now understood that runoff from the fields was not the only source of this surface water flooding and that a vast amount of runoff from Ponsonby Road was a joint contributor.  ',
        "entities": [
            ("DATE", "30th August 2012"),
            ("LOCATION", "Calder Bridge"),
            ("CAUSE", "surface water"),
        ]
    },
    {
        "text": '7 actions have been identified within the report that would minimise the risk of future flooding.  As this report was being drafted, Cumbria County Council Highways has completed a number of actions in response to the flooding.  ',
        "entities": [
        
        ]
    },
    {
        "text": 'cumbria.gov.uk     5 Event Background                                                                                                                                                   ',
        "entities": [
        
        ]
    },
    {
        "text": 'This section describes the location of the flood incident and identifies the properties that were flooded. ',
        "entities": [
        
        ]
    },
    {
        "text": 'Flooding Incident ',
        "entities": [
        
        ]
    },
    {
        "text": '                  Calder Bridge           Figure 1.  Location Plan. ',
        "entities": [
        
        ]
    },
    {
        "text": 'The village of Calder Bridge is on the A595 (see Figure 1. “Location Plan”), at the westerly most corner of the Lake District National Park.  It is about 6km southeast of the town of Egremont and about 3½ km northwest of the village of Gosforth.  The River Calder is crossed by a bridge on the A595 at this location.  About 1km south-east along the A595 is Ponsonby Road, which leads to the settlement of Ponsonby (see Figure 2 “Calder Bridge Surrounding Area Plan”) that consists of farms, cottages and Ponsonby Hall.  Figure 3 “Calder Bridge Village Plan” shows the location of the properties involved in the flooding.  ',
        "entities": [
            ("LOCATION", "Calder Bridge"),
            ("WATERWAY", "River Calder"),
        ]
    },
    {
        "text": 'Calder Bridge  ',
        "entities": [
        
        ]
    },
    {
        "text": ' Cumbria County Council ',
        "entities": [
        
        ]
    },
    {
        "text": '6     Serving the people of Cumbria ',
        "entities": [
        
        ]
    },
    {
        "text": '   Figure 2.  Calder Bridge Surrounding Area Plan ',
        "entities": [
        
        ]
    },
    {
        "text": '    Figure 3.  Calder Bridge Village Plan Waste Wood Beck River Calder Ponsonby Road A595 surface water drain River Calder Calder Bridge bridge A595 Allona The Old Post Office Bridge End Cottage Calder Bridge bridge A595 River Calder ',
        "entities": [
        
        ]
    },
    {
        "text": 'cumbria.gov.uk     7 Investigation This section provides details of the authorities who have contributed to this investigation, an analysis of flow routes and details of likely causes of flooding.  Also included are details of the rainfall event and any previous flooding history in the area. ',
        "entities": [
        
        ]
    },
    {
        "text": 'Rainfall Event ',
        "entities": [
        
        ]
    },
    {
        "text": 'There were many rainfall events during the month of August in 2012, with events lasting almost continuously up to 12 hours.   The weather radar indicated that there was rain almost every day over the Gosforth area in the second half of the month meaning that the ground would be saturated.   ',
        "entities": [
        
        ]
    },
    {
        "text": 'Rainfall radar records indicate that at about 9:00pm on the evening of the 29th August, it started to rain heavily and did not stop until about 3:30am on the 30th August (see Figure 4: “Rainfall Data”).  This rainfall event peaked approximately between 12:45am and 01:05am with an intensity of up to 30mm/hr.      ',
        "entities": [
        
        ]
    },
    {
        "text": 'Figure 4.  Rainfall Data ',
        "entities": [
        
        ]
    },
    {
        "text": '0510152025303509:0010:0011:0012:0001:0002:0003:00Rainfall Intensity (mm/hr)',
        "entities": [
        
        ]
    },
    {
        "text": '29/08/2012 30/08/2012 ',
        "entities": [
        
        ]
    },
    {
        "text": ' Cumbria County Council ',
        "entities": [
        
        ]
    },
    {
        "text": '8     Serving the people of Cumbria ',
        "entities": [
        
        ]
    },
    {
        "text": '    Figure 5.  Radar Picture of Rainfall at Calder Bridge at 1am on 30th August 2012 ',
        "entities": [
        
        ]
    },
    {
        "text": 'The radar picture shown in Figure 5 “Radar picture of Rainfall at Calder Bridge at 1:00am on 30th August 2012”, illustrates that as well as significant rainfall on Calder Bridge, there was an extreme intensity of rainfall on the fells above the village.  Intensities in excess of 50mm/hr. can be seen on this picture.  Intensities above 20mm/hr. are considered to be heavy rainfall. ',
        "entities": [
        
        ]
    },
    {
        "text": 'Calder Bridge ',
        "entities": [
        
        ]
    },
    {
        "text": 'cumbria.gov.uk     9 Map of Flow Routes ',
        "entities": [
        
        ]
    },
    {
        "text": 'There were two properties flooded internally by the extreme rainfall event:  ‘The Old Post Office’ and the adjacent property, ‘Allona’.  A flood incident site investigation was carried out by the Environment Agency (EA), Copeland Borough Council and Cumbria County Council.  ',
        "entities": [
        
        ]
    },
    {
        "text": '       Figure 6.  Calder Bridge Flood Routes Overview    ',
        "entities": [
        
        ]
    },
    {
        "text": 'Although there was considerable flooding of the highway throughout the village, the worst of the flooding occurred on the north side of the River Calder bridge, outside three cottages.  Some of the runoff came from the Sparholme Wood Road (see Figure 6 “Calder Bridge Flood Routes overview”), the junction of which is across the A595 from the cottages, but the main body of the flood came down the A595 from the south.  Flood waters poured off of the fields onto the highway, bringing with it silt and debris, inundating the highway drainage.  There was a considerable amount of runoff coming off of the Ponsonby Road onto the A595.    ‘The Old Post Office’ and ‘Allona’ were flooded internally (see Figure 7: “Calder Bridge Flood Routes and Flood Extent”). ',
        "entities": [
            ("LOCATION", "north side of the River Calder bridge"),
            ("LOCATION", "Sparholme Wood Road"),
            ("LOCATION", "flood came down the A595 from the south"),
            ("CAUSE", "Flood waters poured off of the fields onto the highway"),
            ("CAUSE", "silt and debris, inundating the highway drainage"),
            ("LOCATION", "Ponsonby Road onto the A595"),
        ]
    },
    {
        "text": 'Sparholme Wood Road Calder Bridge bridge flooding from fields flooding along highway flooding along Ponsonby Road A595 ',
        "entities": [
        
        ]
    },
    {
        "text": ' Cumbria County Council ',
        "entities": [
        
        ]
    },
    {
        "text": '10     Serving the people of Cumbria ` ',
        "entities": [
        
        ]
    },
    {
        "text": 'Figure 7.  Calder Bridge Flood Routes and Flood Extent  ',
        "entities": [
        
        ]
    },
    {
        "text": 'Subsequent to the August 2012 flooding, the owners of ‘Allona’ have carried out some resilience measures with flood doors being fitted.  Also County Highways carried out some intensive gully and highway drainage maintenance.  Grips were installed in order to shed extreme flooding off of the highway before the flow reached the village.  However there have been seven flood incidents since August 2012, including one as recent as February 2013 and although ‘Allona’ and ‘Bridge End Cottage’ remained unscathed by the flooding, ‘The Old Post Office’ suffered flooding again.  Once again water poured off of the fields onto the A595 and the highway drainage was overwhelmed. ',
        "entities": [
            
        ]
    },
    {
        "text": 'The gullies along Ponsonby Road feeding the highway drainage on the A595 start at ‘Northwaite’, a cottage just west of the start of Waste Wood Beck.  Road gullies were provided for Ponsonby Road as far along as Gibb Hill, near Ponsonby.  This latter highway drainage system ran down to a pair of road gullies outside ‘Camomile Cottage’, where it was supposed to discharge through a 225mm dia. plastic pipe into a ditch at the start of Waste Wood Beck.  However the runoff was unable to access any of these road gullies. The flow down Ponsonby Road containing debris and silt from the surrounding fields, included runoff from an area that went all the way back to the brow of the hill beyond the Gibb Hill road junction.  This was the main flow that ran down the A595 to Calder Bridge.  ',
        "entities": [
            ("CAUSE", "runoff was unable to access any of these road gullies"),
            ("LOCATION", "main flow that ran down the A595 to Calder Bridge"),
        ]
    },
    {
        "text": 'Flow from Sparholme Wood Road Approximate extent of main flooded area River Calder bridge Main flood route ',
        "entities": [
        
        ]
    },
    {
        "text": 'cumbria.gov.uk     11 Likely Causes of Flooding ',
        "entities": [
        
        ]
    },
    {
        "text": 'Both cottages that flooded are in Flood Zone 2. Records from the EA show that all three cottages immediately north of the bridge are highly susceptible to surface water flooding and all of the flooding has been from surface water and not from the River Calder. ',
        "entities": [
            ("CAUSE", "all of the flooding has been from surface water and not from the River Calder"),
        ]
    },
    {
        "text": 'The flooding of the 30th August 2012 was caused by an extreme rainfall event falling onto saturated fields.  The excessive runoff from the fields discharged onto the highways and quickly overwhelmed any highway drainage that was not already blocked.  Floodwaters flowed down the A595 causing disruption to the traffic.  Highway drainage is designed to drain the highway and does not have the capacity to cope with excessive surface water from adjacent farm land. ',
        "entities": [
            ("DATE", "30th August 2012"),
            ("CAUSE", "excessive runoff"),
            ("CAUSE", "overwhelmed any highway drainage"),
            ("LOCATION", "Floodwaters flowed down the A595"),
            ("CAUSE", "excessive surface water from adjacent farm land"),
        ]
    },
    {
        "text": 'Subsequent Remedial Work ',
        "entities": [
        
        ]
    },
    {
        "text": 'Since the February 2013 flood event, Cumbria County Council Highways have further investigated the causes of the flooding.  It was already known that the highway drainage outfall pipe into the River Calder, which runs across the garden of ‘Bridge End Cottage’, was undersized and not fit for purpose but access to remedy this was not permitted (see Figure 8 “Site of Calder Bridge Highway Drainage Outfall”). . Following legal proceedings, this existing 150mm dia. outlet was replaced by a 225mm dia. pipe in April 2013.  Holes that had been put into the bridge parapet upstream were insufficient to cope with the runoff and so two 150mm dia. pipes were fitted as overflow outlets to a new 500mm dia. catchpit in the highway.  The catchpit was fitted with a grated cover and the pipes discharged through the downstream bridge parapet wingwall enabling the highway drainage to discharge directly into the river.  Two 225mm dia. pipes have also been installed in the wall extension to the upstream side of the bridge parapet, to provide escape routes for exceedance of the highway drainage system by storm water coming down the A595 highway from the Ponsonby Road junction direction.   ',
        "entities": [
        
        ]
    },
    {
        "text": 'South of the village along the A595, a wide grated channel has now also been put across an access to a field that had excessive runoff that was contributing significantly to the flooding.  Although a large percentage of the flooding was caused by runoff from the fields discharging onto the A595, a high flow of flood water had also been observed coming from Ponsonby Lane.  As discussed previously, this flow extended some distance up the lane with no apparent interception points.  The lane is used extensively for farm traffic has been covered in mud and detritus.  The surface of the lane was cleaned by County Highways and eighteen road gullies full of silt were discovered.  These road gullies, together with their associated drainage, were thoroughly cleaned and a ditch in the verge also cleared out.  This work, together with the newly constructed road gully overflow pipes and the laying of the new outfall across the garden of ‘Bridge End Cottage’, will significantly reduce the risk of future flooding. ',
        "entities": [
        
        ]
    },
    {
        "text": ' Cumbria County Council ',
        "entities": [
        
        ]
    },
    {
        "text": '12     Serving the people of Cumbria However a surface water drain has been discovered, crossing the A595 about 200 metres south of the bridge, coming from the direction of Waste Wood and discharging into the River Calder downstream of the bridge.  This will be investigated by County Highways to examine the potential for the drain to take highway drainage in order to provide additional capacity for the existing system. ',
        "entities": [
        
        ]
    },
    {
        "text": '     Figure 8.  Site of Calder Bridge Highway Drainage Outfall ',
        "entities": [
        
        ]
    },
    {
        "text": 'Flooding History ',
        "entities": [
        
        ]
    },
    {
        "text": 'Records indicate that there is not a flooding history from the River Calder.  However there have been historic problems over the last 10 years with surface water.  The runoff from fields contains silt and debris that has quickly caused blockages in the highway drainage system and has filled gullies.  ',
        "entities": [
        
        ]
    },
    {
        "text": 'cumbria.gov.uk     13 Recommended Actions ',
        "entities": [
        
        ]
    },
    {
        "text": 'Action by ',
        "entities": [
        
        ]
    },
    {
        "text": 'Recommended Action ',
        "entities": [
        
        ]
    },
    {
        "text": 'How Residents If affected by flooding, ensure that property is resilient to future flood events. Consider flood resilience measures. The owner of the old Post Office has already installed flood gates to his property. County Highways/Landowner Ditch has been partially cleaned on Ponsonby road but more capacity needs to be obtained Partially complete County Highways Road gullies and highway drainage cleaned out along A595 and Ponsonby Road. ',
        "entities": [
        
        ]
    },
    {
        "text": 'Completed County Highways Upgrade highway drainage outfall Relaying of outfall pipe across garden of ‘Bridge End Cottage’ has now been completed. County Highways Investigate further flood exceedence routes. Explore condition and catchment of surface water drain crossing A595 200m south of the bridge. LLFA/CBC/EA/Residents/Landowners of fields above village Investigate options for intercepting surface water flows from fields with bunds, cut-off ditches and/or land drains. Open forum for dialogue between the stakeholders on feasibility and possible locations of land drainage options. Farmers Review land management to avoid flooding from fields. Change ploughing directions, heavy vehicle tracking routes, cattle circulation routes, etc.  Seek advice. ',
        "entities": [
        
        ]
    },
    {
        "text": 'http://www.environment-agency.gov.uk/homeandleisure/floods/31644.aspxhttp://www.environment-agency.gov.uk/homeandleisure/floods/31644.aspx',
        "entities": [
        
        ]
    },
    {
        "text": ' Cumbria County Council ',
        "entities": [
        
        ]
    },
    {
        "text": '14     Serving the people of Cumbria ',
        "entities": [
        
        ]
    },
    {
        "text": 'Next Steps ',
        "entities": [
        
        ]
    },
    {
        "text": 'CCC as the LLFA will continue to ensure that any actions identified within the actions table of this report are appropriately taken forward by each Risk Management Authority identified.   Actions will continue to be prioritised through the Making Space for Water process and monitored through regular meetings of the group.  Details of the MSfWG members and summary of related processes are detailed in Appendix 2.   ',
        "entities": [
        
        ]
    },
    {
        "text": 'cumbria.gov.uk     15 Appendices ',
        "entities": [
        
        ]
    },
    {
        "text": 'Appendix 1: Glossary Acronyms  EA  Environment Agency CCC  Cumbria County Council LLFA  Lead Local Flood Authority LFRM  Local Flood Risk Management MSfWG Making Space for Water Group FAG  Flood Action Group ',
        "entities": [
        
        ]
    },
    {
        "text": ' Cumbria County Council ',
        "entities": [
        
        ]
    },
    {
        "text": '16     Serving the people of Cumbria Appendix 2: Summary of Relevant Legislation and Flood Risk Management Authorities ',
        "entities": [
        
        ]
    },
    {
        "text": 'The Flood Risk Regulations 1999 and the Flood and Water Management Act 2010 (the Act) have established Cumbria County Council (CCC) as the Lead Local Flood Authority (LLFA) for Cumbria.  This has placed various responsibilities on CCC including Section 19 of the Act which states:  Section 19 (1) On becoming aware of a flood in its area, a lead local flood authority must, to the extent that it considers it necessary or appropriate, investigate— (a) which risk management authorities have relevant flood risk management functions, and (b) whether each of those risk management authorities has exercised, or is proposing to exercise, those functions in response to the flood. (2) Where an authority carries out an investigation under subsection (1) it must— (a) publish the results of its investigation, and (b) notify any relevant risk management authorities. ',
        "entities": [
            ("LLFA", "Cumbria County Council"),
        ]
    },
    {
        "text": 'A ‘Risk Management Authority’ (RMA) means: (a) the Environment Agency, (b) a lead local flood authority, (c) a district council for an area for which there is no unitary authority, (d) an internal drainage board, (e) a water company, and (f) a highway authority. ',
        "entities": [
        
        ]
    },
    {
        "text": 'The table below summarises the relevant Risk Management Authority and details the various local source of flooding that they will take a lead on.  Flood Source Environment Agency Lead Local Flood Authority District Council Water Company Highway Authority RIVERS      Main river      Ordinary watercourse ',
        "entities": [
        
        ]
    },
    {
        "text": 'SURFACE RUNOFF ',
        "entities": [
        
        ]
    },
    {
        "text": 'Surface water ',
        "entities": [
        
        ]
    },
    {
        "text": 'Surface water on the highway ',
        "entities": [
        
        ]
    },
    {
        "text": 'OTHER      Sewer flooding ',
        "entities": [
        
        ]
    },
    {
        "text": 'The sea      Groundwater      Reservoirs      ',
        "entities": [
        
        ]
    },
    {
        "text": 'cumbria.gov.uk     17 The following information provides a summary of each Risk Management Authority’s roles and responsibilities in relation to flood reporting and investigation.  Government – Defra develop national policies to form the basis of the Environment Agency’s and Cumbria County Council’s work relating to flood risk.  Environment Agency has a strategic overview of all sources of flooding and coastal erosion as defined in the Act.  As part of its role concerning flood investigations this requires providing evidence and advice to support other risk management authorities.  The EA also collates and reviews assessments, maps and plans for local flood risk management (normally undertaken by LLFA).  Lead Local Flood Authorities (LLFAs) – Cumbria County Council is the LLFA for Cumbria.  Part of their role requires them to investigate significant local flooding incidents and publish the results of such investigations.  LLFAs have a duty to determine which risk management authority has relevant powers to investigate flood incidents to help understand how they happened, and whether those authorities have or intend to exercise their powers.  LLFAs work in partnership with communities and flood risk management authorities to maximise knowledge of flood risk to all involved.  This function is carried out at CCC by the Local Flood Risk Management Team.  District and Borough Councils – These organisations perform a significant amount of work relating to flood risk management including providing advice to communities and gathering information on flooding.  Water and Sewerage Companies manage the risk of flooding to water supply and sewerage facilities and the risk to others from the failure of their infrastructure.  They make sure their systems have the appropriate level of resilience to flooding and where frequent and severe flooding occurs they are required to address this through their capital investment plans.  It should also be noted that following the Transfer of Private Sewers Regulations 2011 water and sewerage companies are responsible for a larger number of sewers than prior to the regulation.  Highway Authorities have the lead responsibility for providing and managing highway drainage and certain roadside ditches that they have created under the Highways Act 1980.  The owners of land adjoining a highway also have a common-law duty to maintain ditches to prevent them causing a nuisance to road users.  Flood risk in Cumbria is managed through the Making Space for Water process which involves the cooperation and regular meeting of the Environment Agency, United Utilities, District/Borough Councils and CCC’s Highway and LFRM Teams to develop processes and schemes to minimise flood risk.   The MSfWGs meet approximately 4 times per year to cooperate and work together to improve the flood risk in the vulnerable areas identified in this report by completing the recommended actions.  CCC as LLFA has a responsibility to oversee the delivery of these actions.  Where minor works or quick win schemes can be identified, these will be prioritised and subject to available funding and resources will be carried out as soon as possible.  Any major works requiring capital investment will be considered through the Environment Agency’s Medium Term Plan or a partners own capital investment process. ',
        "entities": [
            ("LLFA", "Cumbria County Council"),
        ]
    },
    {
        "text": ' Cumbria County Council ',
        "entities": [
        
        ]
    },
    {
        "text": '18     Serving the people of Cumbria Flood Action Groups are usually formed by local residents who wish to work together to resolve flooding in their area.  The FAGs are often supported by either CCC or the EA and provide a useful mechanism for residents to forward information to the MSfWG. ',
        "entities": [
        
        ]
    },
    {
        "text": 'cumbria.gov.uk     19 Appendix 3: Useful contacts and links ',
        "entities": [
        
        ]
    },
    {
        "text": 'To report flooding: Incident hotline tel. 0800 80 70 60 (24hrs) Floodline: tel. 0845 988 1188 ',
        "entities": [
        
        ]
    },
    {
        "text": 'Cumbria County Council (Local Flood Risk Management):  lfrm@cumbria.gov.uk, www.cumbria.gov.uk, tel: 01228 221330 ',
        "entities": [
        
        ]
    },
    {
        "text": 'Cumbria County Council (Highways): highways@cumbria.gov.uk, www.cumbria.gov.uk, tel: 0845 609 6609 ',
        "entities": [
        
        ]
    },
    {
        "text": 'Cumbria County Council Neighbourhood Forum: tel. 01946 505022 Cumbria.gov.uk/sayit ',
        "entities": [
        
        ]
    },
    {
        "text": 'United Utilities: tel: 0845 746 2200 ',
        "entities": [
        
        ]
    },
    {
        "text": 'Copeland Borough Council info@copeland.gov.uk, www.copeland.gov.uk, tel. 0845 054 8600 ',
        "entities": [
        
        ]
    },
    {
        "text": 'Flood and Water Management Act 2010: http://www.legislation.gov.uk/ukpga/2010/29/contents ',
        "entities": [
        
        ]
    },
    {
        "text": 'Water Resources Act 1991: http://www.legislation.gov.uk/all?title=water%20resources%20act ',
        "entities": [
        
        ]
    },
    {
        "text": 'Land Drainage Act: http://www.legislation.gov.uk/all?title=land%20drainage%20act ',
        "entities": [
        
        ]
    },
    {
        "text": 'Highways Act 1980: http://www.legislation.gov.uk/all?title=highways%20act ',
        "entities": [
        
        ]
    },
    {
        "text": 'EA – ‘Living on the Edge’ a guide to the rights and responsibilities of riverside occupation: http://www.environment-agency.gov.uk/homeandleisure/floods/31626.aspx ',
        "entities": [
        
        ]
    },
    {
        "text": 'EA – ‘Prepare your property for flooding’ how to reduce flood damage including flood protection products and services: http://www.environment-agency.gov.uk/homeandleisure/floods/31644.aspxmailto:lfrm@cumbria.gov.ukhttp://www.cumbria.gov.uk/mailto:highways@cumbria.gov.ukhttp://www.cumbria.gov.uk/mailto:info@copeland.gov.ukhttp://www.copeland.gov.uk/http://www.legislation.gov.uk/ukpga/2010/29/contentshttp://www.legislation.gov.uk/all?title=water%20resources%20acthttp://www.legislation.gov.uk/all?title=land%20drainage%20acthttp://www.legislation.gov.uk/all?title=highways%20acthttp://www.environment-agency.gov.uk/homeandleisure/floods/31626.aspx',
        "entities": [
        
        ]
    },
    {
        "text": ' Cumbria County Council ',
        "entities": [
        
        ]
    },
    {
        "text": '20     Serving the people of Cumbria ',
        "entities": [
        
        ]
    },
    {
        "text": 'cumbria.gov.uk  ',
        "entities": [
        
        ]
    },
]
