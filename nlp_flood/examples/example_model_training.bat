@set wd=%cd%
@cd %~dp0

@..\env\Scripts\python.exe -u ..\train.py ^
 "..\model" ^
 --training-data "training_data\valid_training_*" ^
 --label "DATE" ^
 -i 10 ^
 --blank ^
 --debug

@pause
@cd %wd%
