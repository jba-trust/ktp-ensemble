@set wd=%cd%
@cd %~dp0

@..\env\Scripts\python.exe -u ..\training_creation.py ^
 "texts\10-Runnymede-Borough-S19-Report.pdf" ^
 "texts\2821-Ryde-Binstead-Flood-Investigation-Report-July-2014.pdf" ^
 "texts\green-street-chorleywood-section-19-flood-investigation-final-report.pdf" ^
 "texts\24152_FIR_High-Street-Flore_Rev-03_190617.pdf" ^
 "texts\Flood-Investigation-Bridge-Close-Edinburgh-Drive-Report.pdf" ^
 --label "LOCATION" ^
 --label "DATE" ^
 --label "SOURCE" ^
 --label "EFFECT" ^
 --label "WATERWAY" ^
 --label "CAUSE" ^
 --label "LLFA" ^
 --output "training_data" ^
 --format "blank_training_{}.txt" ^
 --quiet

@pause
@cd %wd%
