# Text Extraction Examples

## Training Data Creation

The `training_creation.py` scripts creates a template training file for a text document (Currently PDFs or plain text files).

An example usage of `training_creation.py` can be found in the `example_training_data_creation.bat` file.

In this example, blank training data files are created for the reports in the `texts/` subfolder.
These training data files are saved in the `training_data/` subfolder with filenames that begin with `blank_training_`.

They are "empty" in terms of training data, but provide the structure and formatting required by the model.
The `training_creation.py` script (which is used by `example_training_data_creation.bat`) also splits the report text into paragraphs.
It does this in the same way that the model does to extract data, removing this as a manual step.

There are training data files in the `training_data/` subfolder that have been filled in with valid training data.
These have filenames that begin with `valid_training_`.

The training data itself is in the following format:

```python
("LOCATION", "Jubilee Lane")
```

It is surrounded with parentheses, and each element is inside speech marks (`"`) and separated by commas (`,`). 
It starts with the name of the *type* of data point, and is followed by the *exact text* within the paragraph that consitutes that data point.

For the following paragraph:

> The flooding of the 30th August 2012 was caused by an extreme rainfall event falling onto saturated fields. 
> The excessive runoff from the fields discharged onto the highways and quickly overwhelmed any highway drainage that was not already blocked. 
> Floodwaters flowed down the A595 causing disruption to the traffic. 
> Highway drainage is designed to drain the highway and does not have the capacity to cope with excessive surface water from adjacent farm land.

The training data could be as follows:

```python
"entities": [
    ("DATE", "30th August 2012"),
    ("CAUSE", "excessive runoff"),
    ("CAUSE", "overwhelmed any highway drainage"),
    ("LOCATION", "Floodwaters flowed down the A595"),
    ("CAUSE", "excessive surface water from adjacent farm land"),
]
```

There is also an optional third part, which specifies the text's location within the paragraph.
This location is the number of characters through the origin text that the extract begins.
This location is needed if a paragraph has the same text more than once.
To include them all, you need to specify the numerical position of subsequent data.

For the following paragraph:

> 1 property was internally flooded on Heigham Street. This property was flooded on the 27 May 2014 rainfall event. 
> Other properties reported external flooding to roads and gardens. These incidents were reported by the Fire and Rescue Service. 
> 1 property was internally flooded on Long John Hill. This property was flooded on the 27 May 2014 rainfall event.

The training data would as follows. Note the `328` in the final data point, as `27 May 2014` features twice.

```python
"entities": [
    ("LOCATION", "Heigham Street"),
    ("DATE", "27 May 2014"),
    ("LOCATION", "Long John Hill"),
    ("DATE", "27 May 2014", 328),
]
```

## Training Data Validation

The `train.py` script (which is used by `example_training_data_validation.bat`) is used to train a model,
using completed training data files. It also has the option to validate the formatting of the training data files,
without training the model.

The file `example_training_data_validation.bat` validates the filled training data files in the `training_data/` subfolder.
These files' names begin with `filled_training_` and some of them have common formatting errors.

Running `example_training_data_validation.bat` will highlight the formatting errors, 
and correcting them should result in the script returning that there are no problems with the data.

### Usage

```batch
env\Scripts\python.exe -u ..\train.py ^
 "C:\path\to\model" ^
 --training-data "C:\path\to\training\data" ^
 --validate
```

## Model Training

There are some example completed training data files in the `training_data/` subfolder. 
These have filenames that begin with `valid_training_`.

The file `example_model_training.bat` is an example of training a new model.

It can help the efficacy of the training, if new labels are trained one at a time.
You can reuse previous training data in subsequent training stages 
These stages can be broken down as follows:

**Stage 1**:

```batch
env\Scripts\python.exe -u model_training.py ^
 "C:\path\to\model" ^
 --training-data "C:\path\to\training_data\stage_1\*" ^
 --label "SOURCE" ^
 -i 100 ^
 --blank-model ^
 --rename "flood_model" ^
 --debug
```

**Stage 2**:

```batch
env\Scripts\python.exe -u model_training.py ^
 "C:\path\to\model" ^
 --training-data "C:\path\to\training_data\stage_1\*" ^
 --training-data "C:\path\to\training_data\stage_2\*" ^
 --label "SOURCE" ^
 --label "CAUSE" ^
  -i 100 ^
 --debug
```

**Stage 3**:

```batch
env\Scripts\python.exe -u model_training.py ^
 "C:\path\to\model" ^
 --training-data "C:\path\to\training_data\stage_1\*" ^
 --training-data "C:\path\to\training_data\stage_2\*" ^
 --training-data "C:\path\to\training_data\stage_3\*" ^
 --label "SOURCE" ^
 --label "CAUSE" ^
 --label "LOCATION" ^
 --label "DATE" ^
  -i 100 ^
 --debug
```

## Text Extraction

The file `example_text_extraction.bat` is an example of using the model to extract data from other texts.

*More details coming Soon*

## Advanced Text Extraction

The `rfo_gaca.bat` is a more advanced example of using the text extraction.

*More details coming Soon*
