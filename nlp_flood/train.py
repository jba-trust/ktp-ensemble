#!/usr/bin/env python
# coding: utf8

import argparse
import logging
import tracing
import model_training
from typing import Optional
from pathlib import Path


tracing.add_tracing()


def main(model_dir: Path, new_directory: Optional[Path], training_data: list, validation_data: list, labels_to_train: list,
         iterations: int, rename: Optional[str], blank_model: bool, only_validate: bool,
         quiet: bool, debug: bool, trace: bool):
    logging_level = logging.INFO
    if quiet:
        logging_level = logging.ERROR
    if debug:
        logging_level = logging.DEBUG
    if trace:
        logging_level = logging.TRACE
    model_training.main(model_dir, new_directory, training_data, validation_data, labels_to_train, iterations, rename,
                        blank_model, only_validate, logging_level)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Trains a NLP Model", prog="Model Trainer")

    parser.add_argument("--version",
                        action="version", version="%(prog)s v0.14.0")

    # noinspection PyTypeChecker
    parser.add_argument("--move",
                        nargs="?", dest="new_directory",
                        type=Path, default=None,
                        help="A new path to save the resultant model")
    # noinspection PyTypeChecker
    parser.add_argument("--training-data", "--train",
                        action="append", dest="training_data",
                        type=Path,
                        help="Paths to training data")
    # noinspection PyTypeChecker
    parser.add_argument("--test-data", "--test",
                        action="append", dest="validation_data",
                        type=Path,
                        help="Paths to test data")
    parser.add_argument("--label",
                        action="append", dest="labels_to_train",
                        type=str,
                        help="Labels to train the model with; Defaults to all present labels")
    parser.add_argument("--iterations", "-i",
                        nargs="?", dest="iterations",
                        type=int, default=30,
                        help="Number of training iterations")
    parser.add_argument("--rename", "-r",
                        nargs="?", dest="rename",
                        type=str, default=None,
                        help="New model name")
    parser.add_argument("--blank",
                        action="store_true", dest="blank_model",
                        default=False,
                        help="Start with a blank model")
    parser.add_argument("--validate",
                        action="store_true", dest="only_validate",
                        default=False,
                        help="Validate training data without performing any training")

    parser.add_argument("--quiet", "-q",
                        action="store_true", dest="quiet",
                        default=False,
                        help="Stifle nonessential output")
    parser.add_argument("--debug",
                        action="store_true", dest="debug",
                        default=False,
                        help="Print debugging information")
    parser.add_argument("--trace",
                        action="store_true", dest="trace",
                        default=False,
                        help="Print detailed execution information")

    # noinspection PyTypeChecker
    parser.add_argument("model_dir",
                        type=Path,
                        help="The model to train")

    args = parser.parse_args()
    main(**vars(args))
