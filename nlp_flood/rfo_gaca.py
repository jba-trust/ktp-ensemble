#!/usr/bin/env python3
# coding: utf8

from typing import Optional
import openpyxl
import argparse
import logging
import latlon_to_bng
from datetime import datetime
from collections import Counter
from pathlib import Path
from enum import Enum, IntEnum
from openpyxl.utils.cell import column_index_from_string

import flood_extraction
from open_struct import OpenStruct

logging.basicConfig(format="[%(name)s] %(levelname)s: %(message)s")
logger = logging.getLogger("RFO GACA NLP")


class Column(Enum):
    REPORT_YEAR = 'A'
    LLFA = 'B'
    GIS_LLFA = 'U'

    NOTES = 'L'
    REPORT_NAME = 'M'
    REPORT_DIRECTORY = 'Q'
    REPORT_FILENAME = 'R'
    REPORT_DATE = 'F'

    FLOOD_DATE = 'V'
    FLOOD_COORDINATES = 'W'
    FLOOD_ROAD = 'Y'
    FLOOD_LOCALITY = 'X'
    FLOOD_TYPE = 'Z'
    FLOOD_WATERCOURSE = 'AA'
    FLOOD_SOURCE = 'AB'
    FLOOD_CAUSE = 'AC'
    FLOOD_DESCRIPTION = 'AD'
    FLOOD_PROPERTIES = 'AE'
    FLOOD_PROPOSED_ACTIONS = 'AF'

    @property
    def index(self) -> int:
        return column_index_from_string(self.value) - 1

    @property
    def letter(self) -> str:
        return self.value


class FloodSource(IntEnum):
    DRAINAGE = 1
    EPHEMERAL_WATERCOURSE = 2
    MAIN_RIVER = 3
    ORDINARY_WATERCOURSE = 4
    SEA = 5
    SEWER = 6
    OTHER = 7
    UNKNOWN = 8


class FloodCause(IntEnum):
    CHANNEL_CAPACITY_EXCEEDED = 1
    GROUNDWATER = 2
    LOCAL_DRAINAGE = 3
    MECHANICAL_FAILURE = 4
    BRIDGE_OBSTRUCTION = 5
    CHANNEL_OBSTRUCTION = 6
    CULVERT_OBSTRUCTION = 7
    DEBRIS_OBSTRUCTION = 8
    OPERATIONAL_FAILURE = 9
    OVERTOPPING_DEFENCES = 10
    OTHER = 11
    UNKNOWN = 12


config = OpenStruct()
# Model Configuration
config.model_filepath = None
# Spreadsheet Configuration
config.workbook_filepath = None
config.temp_output_filepath = None
config.sheet_name = r"Section 19 - NLP Processed Data"
config.first_row = 4
config.ignore_keywords = ["spreadsheet"]
# Entity Configuration
config.entities_to_extract = {"CAUSE", "SOURCE", "LOCATION", "DATE"}
config.entity_formats = {
    'LOCATION': "({0[0]} {0[1]})",
    'DATE': "{:%Y-%m-%d}",
}


def get_format_string(key):
    return config.entity_formats.get(key, "{}")


def get_weighted_results(results: list, label: str, index: Optional[int] = None):
    if index is not None:
        label_results = [r[label][index] for r in results if r[label]]
    else:
        label_results = [r[label] for r in results if r[label]]
    counts = Counter(label_results)
    return [[x[0], x[1] / len(label_results)] for x in counts.most_common()]


def add_to_cell(row: list, column: Column, value: str):
    existing_cell_value = row[column.index].value or ""
    row[column.index].value = existing_cell_value + value + ", "


def determine_flood_source_rfo_enum(sources: list):
    # TODO: Join weights of similar sources (sources that point to same enum value)
    likely_sources = [x for x in sources if x[1] > 0.3]
    if likely_sources:
        pass
    else:
        return FloodSource.UNKNOWN
    if sources:
        exit(0)
    return None


def determine_flood_cause_rfo_enum(causes: list):
    # TODO: Join weights of similar causes (causes that point to same enum value)
    likely_causes = [x for x in causes if x[1] > 0.3]
    if likely_causes:
        pass
    else:
        return FloodCause.UNKNOWN
    return None


def process_row(row: list) -> bool:
    directory = str(row[Column.REPORT_DIRECTORY.index].value)
    filename = str(row[Column.REPORT_FILENAME.index].value)
    filepath = Path(directory, filename)
    analysis = flood_extraction.analyse(config.model_filepath, config.entities_to_extract, config.entity_formats,
                                        filepath)
    results = analysis.output_results(flood_extraction.OutputType.NONE)

    # Order results' entities by frequency
    coordinates = get_weighted_results(results, "LOCATION", 0)
    addresses = get_weighted_results(results, "LOCATION", 1)
    dates = get_weighted_results(results, "DATE")
    sources = get_weighted_results(results, "SOURCE")
    causes = get_weighted_results(results, "CAUSE")

    # TODO:
    #  Sanitise the strings to remove weird unicode characters from the PDFs, unless the workbook can handle them.
    #  See https://docs.python.org/2/library/unicodedata.html#unicodedata.normalize

    for lat_long, frequency in coordinates:
        if lat_long is None:
            continue
        bng_coordinates = latlon_to_bng.WGS84toOSGB36(*lat_long)
        value = get_format_string("LOCATION").format(bng_coordinates)
        existing_cell_value = row[Column.FLOOD_COORDINATES.index].value or ""
        row[Column.FLOOD_COORDINATES.index].value = existing_cell_value + value + ", "

    for address, frequency in addresses:
        if address is None:
            continue
        address_parts = address.split(",")
        if len(address_parts) > 2:
            region = address_parts[-2]
            street = address_parts[0]
        else:
            region = " ".join(address_parts)
            street = ""

        value = get_format_string("ADDRESS_ROAD").format(street)
        add_to_cell(row, Column.FLOOD_ROAD, value)

        value = get_format_string("ADDRESS_LOCALITY").format(region)
        add_to_cell(row, Column.FLOOD_LOCALITY, value)

    for date, frequency in dates:
        if date is None:
            continue
        if frequency < 0.1:
            continue
        value = get_format_string("DATE").format(date)
        add_to_cell(row, Column.FLOOD_DATE, value)

    for source, frequency in sources[0:3]:
        if frequency < 0.2:
            continue
        value = get_format_string("SOURCE").format(source)
        add_to_cell(row, Column.FLOOD_SOURCE, value)

    for cause, frequency in causes[0:3]:
        if frequency < 0.2:
            continue
        value = get_format_string("CAUSE").format(cause)
        add_to_cell(row, Column.FLOOD_CAUSE, value)

    # value = determine_flood_source_rfo_enum(sources).value
    # if value:
    #     add_to_cell(row, Column.FLOOD_TYPE, str(value))
    # value = determine_flood_cause_rfo_enum(causes).value
    # if value:
    #     add_to_cell(row, Column.FLOOD_TYPE, str(value))

    return bool(coordinates and addresses and dates and sources and causes)


def main(overwrite: bool, first_row: int, model: Path, workbook_filepath: Path, temp_filepath: Path, sheet_name: str,
         ignored_keywords: list, quiet: bool, debug: bool, trace: bool):
    """Go through rows in GACA Spreadsheet and extract information from Section 19s where possible"""
    logging_level = logging.INFO
    if quiet:
        logging_level = logging.ERROR
    if debug:
        logging_level = logging.DEBUG
    if trace:
        logging_level = logging.TRACE
    logger.setLevel(logging_level)
    flood_extraction.logger.setLevel(max(logging_level, flood_extraction.logger.level))

    if workbook_filepath:
        config.workbook_filepath = workbook_filepath
    if temp_filepath:
        config.temp_output_filepath = temp_filepath
    if first_row:
        config.first_row = first_row
    if model:
        config.model_filepath = model
    if ignored_keywords:
        config.ignore_keywords = ignored_keywords
    if sheet_name:
        config.sheet_name = sheet_name

    timestamp = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    logger.info(f"Starting analysis at {timestamp}")
    skipped_reports_already_processed = []
    skipped_reports_no_report = []
    incomplete_reports = []

    book = openpyxl.load_workbook(config.workbook_filepath)
    sheet = book[config.sheet_name]
    for row in sheet.iter_rows(config.first_row):
        notes_text = row[Column.NOTES.index].value or ""
        if any(keyword in notes_text for keyword in config.ignore_keywords):
            logger.debug(f"Skipping row {row[0].row}")
            report_name = row[Column.REPORT_FILENAME.index].value
            skipped_reports_already_processed.append((row[0].row, report_name))
            continue
        if row[Column.REPORT_DIRECTORY.index].value is None:
            logger.debug(f"Skipping row {row[0].row}")
            skipped_reports_no_report.append((row[0].row, None))
            continue
        if row[Column.REPORT_FILENAME.index].value is None:
            logger.debug(f"Skipping row {row[0].row}")
            skipped_reports_no_report.append((row[0].row, None))
            continue
        try:
            logger.info(f"Processing row {row[0].row} of {sheet.max_row}")
            row_processed = process_row(row)
            if not row_processed:
                report_name = row[Column.REPORT_FILENAME.index].value
                incomplete_reports.append((row[0].row, report_name))
        except KeyboardInterrupt:
            logger.setLevel(max(logging.INFO, logger.getEffectiveLevel()))
            logger.info(f"Exiting on row {row[0].row}.")
            break

    output_filepath = config.workbook_filepath
    if not overwrite:
        output_filename = config.workbook_filepath.stem
        output_file_ext = config.workbook_filepath.suffix
        output_filepath = output_filepath.with_name(output_filename + "_nlp" + output_file_ext)
    logger.debug("Saving to %s" % output_filepath)
    book.save(output_filepath)

    logger.debug("Rows skipped due to already being processed:")
    for row_number, report_name in skipped_reports_already_processed:
        logger.debug(f"  * {row_number}: {report_name}")
    logger.debug("Rows skipped due to no reports being present:")
    for row_number, report_name in skipped_reports_no_report:
        logger.debug(f"  * {row_number}: {report_name}")

    logger.info("Rows with incomplete information:")
    for row_number, report_name in incomplete_reports:
        logger.info(f"  * {row_number}: {report_name}")

    timestamp = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    logger.info(f"Ending analysis at {timestamp}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process rows from the EA's RFO. Search Section 19s and update rows "
                                                 "based on extracted data", prog="RFO GACA NLP")

    parser.add_argument("--version",
                        action="version", version="%(prog)s v0.14.0")

    parser.add_argument("--ignore",
                        action="append", dest="ignored_keywords",
                        type=str,
                        help="Keywords to designate a row to be ignored")
    parser.add_argument("--overwrite",
                        action="store_true", dest="overwrite",
                        default=False,
                        help="Save over the current spreadsheet")
    parser.add_argument("--sheet",
                        nargs="?", dest="sheet_name",
                        type=str, default=None,
                        help="Name of the sheet in the workbook to process")
    # noinspection PyTypeChecker
    parser.add_argument("--model",
                        nargs="?", dest="model",
                        type=Path, default=None,
                        help="Filepath for the NLP model")
    parser.add_argument("--first-row", "--first",
                        nargs="?", dest="first_row",
                        type=int, default=config.first_row,
                        help="The row to start processing from")
    # noinspection PyTypeChecker
    parser.add_argument("--temp",
                        nargs="?", dest="temp_filepath",
                        type=Path, default=None,
                        help="Filepath for a temporary data file")
    # noinspection PyTypeChecker
    parser.add_argument("--workbook",
                        nargs="?", dest="workbook_filepath",
                        type=Path, default=None,
                        help="Filepath to the RFO spreadsheet")

    parser.add_argument("--quiet", "-q",
                        action="store_true", dest="quiet",
                        default=False,
                        help="Stifle nonessential output")
    parser.add_argument("--debug",
                        action="store_true", dest="debug",
                        default=False,
                        help="Print debugging information")
    parser.add_argument("--trace",
                        action="store_true", dest="trace",
                        default=False,
                        help="Print detailed execution information")

    args = parser.parse_args()
    main(**vars(args))
