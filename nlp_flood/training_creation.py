#!/usr/bin/env python3
# coding: utf8
from pathlib import Path
import argparse
import re
import logging
import pdf_to_text
from typing import Optional
from open_struct import OpenStruct

logging.basicConfig(format="[%(name)s] %(levelname)s: %(message)s")
logger = logging.getLogger("Training Creation")

config = OpenStruct()
config.output_directory = "training_data"
config.filename_pattern = "training_{}.txt"
config.entity_names = []
config.unknown_as_plain_text = False


def get_report_text(path: Path) -> Optional[str]:
    file_extension = path.suffix
    if file_extension == ".pdf":
        logger.info("Converting pdf -> text")
        return pdf_to_text.convert_pdf(path)
    elif file_extension == ".txt":
        logger.info("Opening plaintext file")
        # noinspection PyTypeChecker
        with open(path, 'r', encoding="utf-8") as input_file:
            return input_file.read()
    # TODO: Add other file-type conversions
    else:
        if config.unknown_as_plain_text:
            logger.info("Treating unknown file type as plaintext file")
            # noinspection PyTypeChecker
            with open(path, 'r', encoding="utf-8") as input_file:
                return input_file.read()
        else:
            message = f"Unknown file type '{file_extension}'. To treat this as plaintext use the --plain flag."
            logger.warning(message)
            return None


def split_text_into_paragraphs(text: str) -> list:
    paras = re.split(r"\n\s*\n\s*\n", text)
    paras = [x for x in paras if x != ""]
    paras = [x.replace("\n", "") for x in paras]
    return paras


def create_training_file(paragraphs: list, report_filepath: Path):
    output_dir = config.output_directory or Path(".")
    output_name = Path(config.filename_pattern.format(report_filepath.stem).replace(" ", "_"))
    filepath = output_dir / output_name
    logger.info(f"Outputting to '{filepath}'.")
    # noinspection PyTypeChecker
    with open(filepath, 'w', encoding="utf8") as output_file:
        output_file.write("# coding: utf8\n")
        output_file.write("\n")
        output_file.write("# All entities:\n")
        for entity in config.entity_names:
            output_file.write("#   - {}\n".format(entity))
        output_file.write("\n")
        output_file.write(f"# Training Data for {report_filepath.stem}\n")
        output_file.write(f"# {report_filepath}\n")
        output_file.write("[\n")
        for para in paragraphs:
            output_file.write("    {\n")
            output_file.write("        \"text\": '{}',\n".format(para.replace("'", "\\'")))
            output_file.write("        \"entities\": [\n")
            output_file.write("        \n")
            output_file.write("        ]\n")
            output_file.write("    },\n")
        output_file.write("]\n")


def main(labels: list, output_path: Path, report_paths: list, unknown_as_plaintext: bool, file_format: str,
         quiet: bool, debug: bool, trace: bool):
    """Create empty training files for reports"""
    logging_level = logging.INFO
    logging_level = logging.INFO
    if quiet:
        logging_level = logging.ERROR
    if debug:
        logging_level = logging.DEBUG
    if trace:
        logging_level = logging.TRACE
    logger.setLevel(logging_level)

    if labels:
        config.entity_names = labels
    if output_path:
        config.output_directory = output_path
    if file_format:
        config.filename_pattern = file_format
    if unknown_as_plaintext is not None:
        config.unknown_as_plain_text = unknown_as_plaintext

    try:
        for report_path in report_paths:
            if not report_path.is_file():
                logger.error(f"Could not find file {report_path}")
                continue
            text = get_report_text(report_path)
            if text is None:
                continue
            paragraphs = split_text_into_paragraphs(text)
            create_training_file(paragraphs, report_path)
    except KeyboardInterrupt:
        logger.warning("Interrupted")
        return


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Creates formatted blank training data", prog="Training Data Creation")

    parser.add_argument("--version",
                        action="version", version="%(prog)s v0.2.0")

    # noinspection PyTypeChecker
    parser.add_argument("--output",
                        nargs="?", dest="output_path",
                        type=Path, default=None,
                        help="The path to the output file; The file will be created if it doesn't exist, and will be "
                             "overwritten if it does")
    parser.add_argument("--format",
                        nargs="?", dest="file_format",
                        type=str, default=None,
                        help="The filename format for generated training files")
    parser.add_argument("--plain", "-p",
                        action="store_true", dest="unknown_as_plaintext",
                        default=False,
                        help="Read any unknown filetypes as plaintext.")
    parser.add_argument("--label",
                        action="append", dest="labels",
                        type=str,
                        help="Labels to provide as hints")

    parser.add_argument("--quiet", "-q",
                        action="store_true", dest="quiet",
                        default=False,
                        help="Stifle nonessential output")
    parser.add_argument("--debug",
                        action="store_true", dest="debug",
                        default=False,
                        help="Print debugging information")
    parser.add_argument("--trace",
                        action="store_true", dest="trace",
                        default=False,
                        help="Print detailed execution information")

    # noinspection PyTypeChecker
    parser.add_argument("report_paths",
                        nargs="*",
                        type=Path,
                        help="The texts to use as training data")

    args = parser.parse_args()
    main(**vars(args))
