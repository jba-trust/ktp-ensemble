# coding: utf8

"""
Reference: https://spacy.io/usage/training#example-new-entity-type

Note to users when creating training data files.
Entities will be searched for through the text, and it's possible that the text of the entity could be found earlier in
the text. If this happens and it's not what you want, you can specify a starting index for the entity's text.

"""
import logging
import glob
import random
import ast
from open_struct import OpenStruct
from typing import Optional
from datetime import datetime
from pathlib import Path
import spacy
from spacy.util import minibatch, compounding


TRAINING_PERFORMANCE_FILENAME = Path("performance.txt")


logging.basicConfig(format="[%(name)s] %(levelname)s: %(message)s")
logger = logging.getLogger("Model Training")


config = OpenStruct()
config.exit_on_invalid_data = True
config.error_context_width = 30


def split_lines(text: str) -> str:
    for line in text.split("\n"):
        yield line


def get_data_line(data_text: str, file_text: str) -> int:
    first_line = data_text.split("\n")[0]
    for i, line in enumerate(split_lines(file_text)):
        # Convert escaped apostrophes back to apostrophes, as the the loaded data has already done this, and they
        # won't match otherwise.
        if line.replace("\\'", "\'").find(first_line) >= 0:
            return i
    return -1


def validate_training_datum(datum: dict, filename: Path, contents: str):
    """Validates provided data and ensures it's in the correct format.

    Parameters
    ----------
    datum : dict
        A dictionary holding text extract and a list of entity tuples.
        The entity tuple is the entity name, the text that is that entity,
        and an optional starting position to find the subtext in the surrounding text.
    filename : Path
        The filename of training data that this data comes from.
    contents : str
        The contents of the training data file
    """
    invalid = False
    text, entities = datum['text'], datum['entities']
    line_index = get_data_line(text, contents)
    for entity in entities:
        if len(entity) < 2:
            logger.error(f"The data provided '{entity}' is invalid.")
            logger.error("It may be missing a comma. It needs to be in the format (\"ENTITY_NAME\", \"some text\"),\n")
            logger.error(f"On line {line_index} in {filename}.")
            invalid = True
        elif entity[1] not in text:
            logger.error(f"The text '{entity[1]}' could not be found in the surrounding text.")
            logger.error("It must match exactly as it is originally written.")
            logger.error("There may be a typo, or the PDF may have used characters that are invisible or look similar "
                         "to normal letters. ")
            logger.error("In this instance, copy and paste from the text to maintain these characters.")
            logger.error(f"The original text begins \"{text[0:64]}...\".")
            logger.error(f"On line {line_index} in {filename}.")
            invalid = True
    if invalid and config.exit_on_invalid_data:
        exit(1)


def load_training_datum(datum: dict, labels_to_include: Optional[set], filename: Path, contents: str) -> (str, dict):
    """Converts from convenient-to-write training data into readable-by-spaCy training data.

    Parameters
    ----------
    datum : dict
        A dictionary holding text extract and a list of entity tuples.
        The entity tuple is the entity name, the text that is that entity,
        and an optional starting position to find the subtext in the surrounding text.
    labels_to_include : Optional[set]
        If this is None, then all labels will be included.
        If this is a set, then only the labels in the collection will be included.
        If this is an empty set, no labels will be included.
    filename : Path
        The filename of training data that this data comes from.
    contents : str
        The contents of the training data file
    """
    validate_training_datum(datum, filename, contents)
    text, entities = datum['text'], datum['entities']
    # Check for missing subtexts in larger text
    entities = [x for x in entities if len(x) >= 2]
    entities = [(text.find(x[1], x[2] if len(x) > 2 else 0),
                 text.find(x[1], x[2] if len(x) > 2 else 0) + len(x[1]),
                 x[0])
                for x in entities]
    # Remove missing subtexts from entity list
    entities = [x for x in entities if x[0] > -1]
    # Remove leading and trailing whitespace from text
    trimmed_entities = []
    for i, entity in enumerate(entities):
        start = entity[0]
        while start < len(text) and text[start].isspace():
            start += 1
        end = entity[1]
        while end > 1 and text[end-1].isspace():
            end -= 1
        trimmed_entities.append([start, end, *entity[2:]])
    entities = [x for x in trimmed_entities if x[0] < x[1]]
    if labels_to_include is not None:
        entities = [x for x in entities if x[2] in labels_to_include]
    return text, {"entities": entities}


def handle_invalid_syntax(error: Exception, filename: str, contents: str):
    logger.error("There is an error with how the provided data is laid out.")
    logger.error("")
    if type(error) is SyntaxError:
        if error.offset == len(error.text):
            handle_missing_quotation_marks(error, filename, contents)
            return
        error_line = contents.split("\n")[error.lineno - 1]
        next_character = error.text[error.offset]
        first_invalid_character = error.text[error.offset - 1]
        last_valid_character = error.text[error.offset - 2]
        quotation_mark_count = error_line.count("\"") + error_line.count("\'")
        if first_invalid_character == ":" and last_valid_character in ["'", "\""]:
            handle_missing_comma(error, filename, contents)
        elif error_line.count("(") < 1 or error_line.count(")") < 1:
            handle_missing_parentheses(error, filename, contents)
        elif error_line.count("\"", 0, error.offset) + error_line.count("\'", 0, error.offset) < 4:
            handle_missing_quotation_marks(error, filename, contents)
        elif quotation_mark_count >= 4:
            handle_unescaped_quotation_mark(error, filename, contents)
        else:
            error_context_start = max(0, error.offset - config.error_context_width)
            error_content_end = min(error.offset + config.error_context_width, len(error.text),
                                    error.text.index("\n", error.offset))
            error_context = error.text[error_context_start:error_content_end]
            logger.error("An unknown error occurred. ")
            logger.error("The data is formatted as a Python object.")
            logger.error("If you know anyone who can help with Python issues, they should be able")
            logger.error("to find the problem. Otherwise, you can try recreating the training data")
            logger.error("from a blank training data file.")
            logger.error("")
            logger.error(error_context)
            logger.error("")
            logger.error(f"On line {error.lineno} in {filename}.")
    elif type(error) is ValueError:
        handle_missing_comma(error, filename, contents)
    elif type(error) is TypeError:
        handle_missing_comma(error, filename, contents)
    if config.exit_on_invalid_data:
        exit(1)


def handle_missing_quotation_marks(error: Exception, filename: str, contents: str):
    """Missing quotation mark around either entity or text extract

    Eg. (CAUSE", "surface water"),
    -----^ (missing speech mark!)

    Eg. ("CAUSE, "surface water"),
    -----------^ (missing speech mark!)

    Eg. ("CAUSE", surface water"),
    --------------^ (missing speech mark!)

    Eg. ("CAUSE", "surface water),
    ----------------------------^ (missing speech mark!)
    """
    error_line = contents.split("\n")[error.lineno - 1]
    offset = max(error_line.rfind("\"", 0, error.offset), 
                 error_line.rfind("'", 0, error.offset))
    first_speech_mark_position  = error_line.find("(") + 1
    second_speech_mark_position = error_line.find(",")
    third_speech_mark_position  = error_line.find(",") + 1
    fourth_speech_mark_position = error_line.rfind(")")

    if not error_line[first_speech_mark_position:].strip().startswith("\""):
        offset = first_speech_mark_position
    elif not error_line[:second_speech_mark_position].strip().endswith("\""):
        offset = second_speech_mark_position
    elif not error_line[third_speech_mark_position:].strip().startswith("\""):
        offset = third_speech_mark_position
    elif not error_line[fourth_speech_mark_position].strip().endswith("\""):
        offset = fourth_speech_mark_position

    error_context_start = max(0, offset - config.error_context_width)
    error_content_end = min(offset + config.error_context_width, len(error.text),
                            error.text.index("\n", offset))
    error_context = error.text[error_context_start:error_content_end]
    error_identification = '-' * (offset - error_context_start) + '^'
    logger.error("It seems that either the entity type or the text extract is missing")
    logger.error("either a closing or opening speech mark. Make sure there are speech")
    logger.error("marks (\") around the entity name, and the text extract.")
    logger.error("")
    logger.error(f"[...]{error_context}[...]")
    logger.error(f"-----{error_identification}")
    logger.error("")


def handle_missing_comma(error: Exception, filename: str, contents: str):
    if type(error) is SyntaxError:
        """Missing comma at end of paragraph line
        
        Eg. [...]end of paragraph text.'
        --------------------------------^ (no comma!)
        """
        error_lineno = error.lineno - 1
        previous_line = contents.split("\n")[error_lineno - 1]
        error_context = previous_line[-config.error_context_width:]
        error_identification = '-' * config.error_context_width + '^'
        logger.error("It appears the preceding line is missing a comma.")
        logger.error("")
        logger.error(f"[...]{error_context}")
        logger.error(f"-----{error_identification}")
        logger.error("")
        logger.error(f"On line {error_lineno} in {filename}.")
    elif type(error) is TypeError:
        """Missing comma in between entity and text extract
        
        Eg. ("CAUSE" "surface water"),
        ------------^ (no comma!)
        """
        error_lines = [[x.strip().find("\"", 2), x, i+1] for i, x in enumerate(contents.split("\n")) if x.strip().startswith("(")]
        error_lines = [[a, x.strip().find("\"", a + 1), x, i] for a, x, i in error_lines]
        error_lines = [[x, i, a + 1] for a, b, x, i in error_lines if a > -1 and b > -1 and "," not in x.strip()[a:b]]
        if error_lines:
            for error_line, line_number, offset in error_lines:
                error_context_start = max(0, offset - config.error_context_width)
                error_content_end = min(offset + config.error_context_width, len(error_line),
                                    error_line.find("\n", offset))
                error_context = error_line.strip()[error_context_start:error_content_end]
                error_identification = '-' * (offset - error_context_start) + '^'
                logger.error("This line is missing a comma between the entity name and the text")
                logger.error("extract.")
                logger.error("")
                logger.error(f"[...]{error_context}[...]")
                logger.error(f"-----{error_identification}")
                logger.error("")
                logger.error(f"On line {error_line} in {filename}.")
        else:
            raise error
    elif type(error) is ValueError:
        """Missing comma at end of previous data point line
        
        Eg. ("CAUSE", "surface water")
        ------------------------------^ (no comma!)
        """
        missing_comma = contents.find(")\n")
        error_lineno = "<unknown line>"
        logger.error("It appears there is a missing comma at the end of a data point.")
        if missing_comma > -1:
            error_lineno = contents.count("\n", 0, missing_comma) + 1
            error_context = contents.split("\n")[error_lineno - 1][-config.error_context_width:]
            error_identification = '-' * config.error_context_width + '^'
            logger.error("")
            logger.error(f"[...]{error_context}")
            logger.error(f"-----{error_identification}")
        logger.error("")
        logger.error(f"On line {error_lineno} in {filename}.")


def handle_unescaped_quotation_mark(error: Exception, filename: str, contents: str):
    """Unescaped quotation mark in text entity
    
    Eg. ("CAUSE", "surface" water"),
    ----------------------^ (extra speech mark!)
    """
    error_line = contents.split("\n")[error.lineno - 1]
    offset = max(error_line.rfind("\"", 0, error.offset), 
                 error_line.rfind("'", 0, error.offset))
    error_context_start = max(0, offset - config.error_context_width)
    error_content_end = min(offset + config.error_context_width, len(error.text),
                            error.text.index("\n", offset))
    error_context = error.text[error_context_start:error_content_end]
    error_identification = '-' * (offset - error_context_start) + '^'
    logger.error("It appears that there is an unescaped quotation mark or speech mark.")
    logger.error("To solve this, add a backslash (\\) immediately before the quotation")
    logger.error("mark.")
    logger.error("")
    logger.error(f"[...]{error_context}[...]")
    logger.error(f"-----{error_identification}")
    logger.error("")
    logger.error(f"On line {error.lineno} in {filename}.")


def handle_missing_parentheses(error: Exception, filename: str, contents: str):
    """Unescaped opening/closing parentheses in data point
    
    Eg. "CAUSE", "surface water"),
    ----^ (missing opening bracket!)

    Eg. ("CAUSE", "surface water",
    -----------------------------^ (missing opening bracket!)
    """
    error_line = contents.split("\n")[error.lineno - 1]
    opening_paren_position = error_line.find(error_line.strip()[0])
    closing_paren_position = error_line.find(error_line.strip()[0])
    offset = opening_paren_position - 1
    missing_close_paren = contents.find("\",\n")
    if missing_close_paren > -1:
        error_lineno = contents.count("\n", 0, missing_close_paren) + 1
        error_line = contents.split("\n")[error_lineno - 1]
        offset = error_line.rfind(",")
    error_identification = '-' * offset + '^'
    logger.error("This line is missing opening and/or closing brackets.")
    logger.error("")
    logger.error(f"{error_line}")
    logger.error(f"{error_identification}")
    logger.error("")
    logger.error(f"On line {error.lineno} in {filename}.")


def load_training_data(data_filepaths: list, labels_to_include: Optional[set]) -> list:
    data = []
    for filepath in data_filepaths:
        for filename in glob.glob(str(filepath)):
            logger.debug(f"Loading training data from {filename}.")
            with open(filename, 'r', encoding="utf-8") as data_file:
                contents = data_file.read()
                try:
                    parsed_data = ast.literal_eval(contents)
                except (SyntaxError, ValueError) as error:
                    handle_invalid_syntax(error, filename, contents)
                else:
                    try:
                        data.extend([(load_training_datum(x, labels_to_include, filename, contents)) for x in parsed_data])
                    except TypeError as error:
                        handle_invalid_syntax(error, filename, contents)
    return data


def setup_training(model, from_empty_model: bool, training_data_filepaths: list, labels_to_train: set = None):
    labels_trained = set()
    training_data = load_training_data(training_data_filepaths, labels_to_train)
    for datum in training_data:
        for entity in datum[1]['entities']:
            labels_trained.add(entity[2])

    ner = model.get_pipe("ner")
    for label in labels_trained:
        ner.add_label(label)

    if from_empty_model:
        optimiser = model.begin_training()
    else:
        optimiser = model.resume_training()
    return training_data, optimiser


def train_model(model, blank_model: bool, training_paths: list, labels_to_train: set, iterations: int):
    training_data, optimiser = setup_training(model, blank_model, training_paths, labels_to_train)
    if not training_data:
        return
    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in model.pipe_names if pipe != "ner"]
    with model.disable_pipes(*other_pipes):  # only train NER
        sizes = compounding(1.0, 4.0, 1.001)
        # batch up the examples using spaCy's minibatch
        time_now = datetime.now().strftime("%H:%M:%S")
        logger.info(f"Training beginning at {time_now}.")
        for i in range(iterations):
            logger.info(f"Round {i+1} of {iterations}")
            try:
                random.shuffle(training_data)
                batches = minibatch(training_data, size=sizes)
                losses = {}
                for batch in batches:
                    texts, annotations = zip(*batch)
                    model.update(texts, annotations, sgd=optimiser, drop=0.35, losses=losses)
                # Not sure on how function calculates the losses.
                logger.debug(f"Losses {losses['ner']}")
            except KeyboardInterrupt:
                logger.setLevel(max(logger.getEffectiveLevel(), logging.INFO))
                logger.info("Interrupted by user.")
                break
        time_now = datetime.now().strftime("%H:%M:%S")
        logger.info(f"Training ending at {time_now}.")


def validate_model(model, model_dir: Path, test_paths: list, labels_to_check: set):
    validation_data = load_training_data(test_paths, labels_to_check)
    stats = {
        "Made up": [],
        "Missed": [],
    }
    total = 0
    for text, obj in validation_data:
        try:
            expected = obj['entities']
            total += len(expected)
            missed = [*expected]
            doc = model(text)
            for ent in doc.ents:
                match = (ent.start_char, ent.end_char, ent.label_)
                if match in expected:
                    missed.remove(match)
                    logger.debug(f"  - Model found {ent.label_} ('{ent.text}')")
                else:
                    stats["Made up"].append(ent)
                    logger.debug(f"  - Model made up {ent.label_} ('{ent.text}')")
            for ent in missed:
                label = ent[-1]
                text = text[ent[0]:ent[1]]
                logger.debug(f" - Model missed {label} ('{text}')")
            for x in missed:
                logger.debug(x)
            stats["Missed"].extend([doc.char_span(*x) for x in missed])
        except KeyboardInterrupt:
            logger.setLevel(max(logger.getEffectiveLevel(), logging.INFO))
            logger.info("Interrupted by user.")
            break
    if total > 0:
        update_performance_record(model_dir, stats, total)


def update_performance_record(model_dir: Path, stats: dict, total: int):
    logger.info(f"Of {total} expected datapoints to find:")
    for stat in stats:
        value = stats[stat]
        n = len(value)
        percentage = 100 * n / total
        logger.info(f"{stat} {n} ({percentage}%)")
    performance_filepath = model_dir/TRAINING_PERFORMANCE_FILENAME
    with open(performance_filepath, 'a', encoding="utf-8") as file:
        missed = stats["Missed"]
        made_up = stats["Made up"]
        file.write(f"{total}: +{made_up}, -{missed}")


def save_model(model, output_dir: Path, output_name: str):
    if output_dir is None:
        return None
    if not output_dir.exists():
        output_dir.mkdir()
    model.meta["name"] = output_name
    model.to_disk(output_dir)
    logger.info(f"Saved model '{output_name}' to {output_dir}")

    performance_filepath = output_dir/TRAINING_PERFORMANCE_FILENAME
    if not performance_filepath.is_file():
        with open(performance_filepath, 'w', encoding="utf-8") as file:
            file.write("{total_training_entities}: +{made_up_entities}, -{missed_entities}")

    # return the saved model
    saved_model = spacy.load(output_dir)
    logger.info(f"Loaded from {output_dir}")
    return saved_model

#TODO: Replace blank model with "en_core_web_sm"
def create_model(blank_model: bool, model_path: Path):
    if blank_model:
        model = spacy.blank("en")
        model.add_pipe(model.create_pipe('parser'))
        model.add_pipe(model.create_pipe("ner"))
        model.begin_training()
        logger.info("Created blank 'en' model")
    else:
        model = spacy.load(model_path)
        logger.info(f"Loaded model {model_path}")
        if "ner" not in model.pipe_names:
            model.add_pipe(model.create_pipe("ner"))
            logger.warning("Had to add an 'NER' pipe to the model.")
        if "parser" not in model.pipe_names:
            model.add_pipe(model.create_pipe('parser'))
            logger.warning("Had to add a 'parser' pipe to the model.")
    return model


def main(model_dir: Path, new_dir: Optional[Path], training_data: list, validation_data: list, labels_to_train: list,
         iterations, rename: Optional[str], blank_model: bool, only_validate: bool, logging_level=logging.INFO):
    """Set up the pipeline and entity recognizer, and train the new entity"""
    logger.setLevel(logging_level)
    # TODO: Allow passing of exit_on_invalid_data

    if only_validate:
        load_training_data(training_data, None)
        logger.setLevel(max(logger.getEffectiveLevel(), logging.INFO))
        logger.info("No problems found with supplied training data.")
        exit(0)

    model = create_model(blank_model, model_dir)

    if labels_to_train is not None:
        labels_to_train = set(labels_to_train)
        logger.info("Restricting training to [{}] entities.".format(", ".join(labels_to_train)))

    if new_dir is not None:
        saved_model = save_model(model, new_dir, rename or model.name)
        # Check the classes have loaded back consistently
        assert saved_model.get_pipe("ner").move_names == model.get_pipe("ner").move_names

    if training_data:
        train_model(model, blank_model, training_data, labels_to_train, iterations)
        logger.info("Testing against training data")
        validate_model(model, new_dir or model_dir, training_data, labels_to_train)

    if validation_data:
        logger.info("Testing against validation data")
        validate_model(model, new_dir or model_dir, validation_data, labels_to_train)
