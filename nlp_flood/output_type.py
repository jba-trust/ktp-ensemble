from enum import Enum

class OutputType(Enum):
    HTML = "html"
    CSV  = "csv"
    NONE = "none"

    @staticmethod
    def values() -> set:
        return set([x.value for x in OutputType])

    @staticmethod
    def from_string(s):
        try:
            return OutputType(s)
        except KeyError:
            raise ValueError()
