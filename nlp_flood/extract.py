#!/usr/bin/env python
# coding: utf8

import argparse
import logging
import tracing
import flood_extraction
from typing import Optional
from pathlib import Path
from output_type import OutputType


tracing.add_tracing()


def main(model: Path, paths: list, labels: list, country_hint: str = None, provenance_filepath: Path = None,
         output_path: Optional[Path] = None, output_type: OutputType = OutputType.CSV,
         quiet: bool = False, debug: bool = False, trace: bool = False):
    """Extract flood data from naturally-written documents"""
    logging_level = logging.INFO
    if quiet:
        logging_level = logging.ERROR
    if debug:
        logging_level = logging.DEBUG
    if trace:
        logging_level = logging.TRACE
    flood_extraction.logger.setLevel(logging_level)

    if country_hint is not None:
        flood_extraction.config.country_hint = country_hint
    if provenance_filepath is not None:
        flood_extraction.config.provenance_filepath = provenance_filepath
    analysis = flood_extraction.analyse(model, set(labels), None, *paths)
    analysis.output_results(output_type, output_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Extracts data from text", prog="Text Extractor")
    
    parser.add_argument("--version",
                        action="version", version="%(prog)s v0.14.0")

    parser.add_argument("--label",
                        action="append", dest="labels",
                        type=str,
                        help="The labels to extract")
    parser.add_argument("--output-type",
                        nargs="?", dest="output_type",
                        type=OutputType.from_string, default=OutputType.CSV,
                        help="How to format the output")
    # noinspection PyTypeChecker
    parser.add_argument("--output-path",
                        nargs="?", dest="output_path",
                        type=Path, default=None,
                        help="The path to the output file; The file will be created if it doesn't exist, and will be "
                             "overwritten if it does")
    parser.add_argument("--country",
                        nargs="?", dest="country_hint",
                        type=str, default=None,
                        help="The country code to use in the geocoding process. This should be a two-character ccTLD.")
    # noinspection PyTypeChecker
    parser.add_argument("--provenance", "-p",
                        nargs="?", dest="provenance_filepath",
                        type=Path, default=None,
                        help="The path to the output file for any provenance data during extraction. The file will be "
                             "created if it doens't exist, and will be appended to if it does.")

    parser.add_argument("--quiet", "-q",
                        action="store_true", dest="quiet",
                        default=False,
                        help="Stifle nonessential output")
    parser.add_argument("--debug",
                        action="store_true", dest="debug",
                        default=False,
                        help="Print debugging information")
    parser.add_argument("--trace",
                        action="store_true", dest="trace",
                        default=False,
                        help="Print detailed execution information")

    # noinspection PyTypeChecker
    parser.add_argument("model",
                        type=Path,
                        help="Filepath to NLP model")
    # noinspection PyTypeChecker
    parser.add_argument("paths",
                        nargs="+",
                        type=Path,
                        help="Filepaths to the input texts")

    args = parser.parse_args()
    main(**vars(args))
