from tika import parser
import logging
from pathlib import Path

logging.basicConfig(format="[%(name)s] %(levelname)s: %(message)s")
logger = logging.getLogger("PDF to Text")


def convert_pdf(pdf_filepath: Path) -> str:
    try:
        parsed = parser.from_file(str(pdf_filepath))
    except UnicodeEncodeError as err:
        logger.error(f"{pdf_filepath} contained an unencodable character.")
        logger.error(err)
        return ""
    plain_text = parsed["content"]
    if plain_text is None:
        logger.error(f"{pdf_filepath} contained no readable text.")
        return ""
    if plain_text.strip() == "":
        logger.error(f"{pdf_filepath} contained no readable text.")
    return plain_text
