# Ontology Design Guide

## Ontology Overview

The ontology design is based upon a source-pathway-receptor (hazard-vulnerability-impacts) 
approach to flood report modelling. The aim is to encompass all types of models and reports
with an ontology that is both fit for current use/test cases and equally appropriate for future
additions where necessary.

![OntologyDesign](DataSchemaSketch.png)
#### Figure 1: *Ontology design diagram showing the relationships between classes and subclasses*
Image to be changed

## Ontology Breakdown

The ontology displayed in **Figure 1 (add reference)** shows the class structure and how each core
class is connected to one another. This following section breaks this down further explaining
briefly the design of each class as well as the corresponding data that would be expected within
them. The list of datatypes/datalabels is non-exhaustive to account for future extensions that could
be made.

It should be noted that any instance within the "ontology" does not need information for each of the classes.
E.g. A specific property may have an address, a postcode and corresponding longitude and latitude. It doesnt necessarily
need any other information such as "report" or other metadata. This could be considered analagous to a row 

#### Table explanation

The tables in the following sections are focusing on specific parts of the ontology. Each key class, 
subclasses, object properties and data properties are described in a 2d heirarchical view. **"Reference type"**
refers to the types described above. **"Label"** is the name of the class/object property. **"Parent Class/Property"**
is referring to the parent class or property which the subclass inherits from. **"Description"** explains the
purpose of the class/property and its use.


#### Risk Classes

The ontology is designed such that risks can be described by a sub-classes that explicitly state
the modellers interpretation of their risk as broad categories whilst including the model-specific 
description of the risk. E.g. A modeller describes one level of risk being 'medium', in terms of probability
this may refer to an upper bound of 1 in 30 AEP and a lower bound of 1 in 100 AEP.
Using both descriptions allows different interpretations of risk to be evaluated in queries and also allows
for similarity searches to be made easily.

Reference Type|Label|Parent Class/Property|Description|
---|---|---|---
|Class|Risk|---|The parent class to all risk levels. Used to refer to a risk level that is associated with a Place (or Location)|
|---|VeryLowRisk|Risk|A class associated with very low risk levels where very low risk is defined by the data author|
|---|LowRisk|Risk|A class associated with low risk levels where low risk is defined by the data author|
|---|MediumRisk|Risk|A class associated with medium risk levels where medium risk is defined by the data author|
|---|HighRisk|Risk|A class associated with high risk levels where high risk is defined by the data author|
|---|VeryHighRisk|Risk|A class associated with very high risk levels where very high risk is defined by the data author|
|Data Property|riskLowerBound|---|A data property of Risk which all subclasses inherit. The lower bound refers to the least probable of the risk bounds (less frequent occurrence probability). The data should be a non-negative integer.|
|---|riskUpperBound|---|A data property of Risk which all subclasses inherit. The upper bound refers to the most probable of the risk bounds (more frequent occurrence probability). The data should be a non-negative integer.|
|---|riskDefinition|---|A data property of Risk which all subclasses inherit. This is a descriptive string describing how risk is defined for this instance.|

#### Metadata classes (referring to documentation and data origin)
TODO: Rename report to document and have report etc. as sub-classes
One of the more obscurely named classes is 'Report'. It is meant to represent the document type and origin/provenance information about the data provided via the document. The only direct sub-class of this
is 'Provenance' which is in turn a parent for all sub-classes such as 'Author', 'PublishDate' and so on. These sub-classes all have 'hasValue' data properties. The use of sub-classes with 'hasValue' data
properties was to support a variety of queries. Using the sub-classes e.g. 'Author' means we could search for all values (names) that would be of type 'Author'. Similarly, we may want to obtain all
provenance information not just Author. This is achieved but using the fact that provenance is the parent class of the provenance subtype (such as 'Author'), meaning were we to request all 'hasValue' properties
of Provenance then all sub-class information will be returned. As a result the database is now capable of metadata queries.


|Reference Type|Label|Parent Class/Property|Description|
---| --- | --- | ---
|Class|Report|---|Data/Information Source. Can refer to the metadata description of a dataset, Section-19 report or any other related article including mined such as twitter data.
|---|Provenance|Report|Provenance information referring to the report document|
|---|Author|Provenance|Data author e.g. an organisation or an individual|
|---|PublishDate|Provenance|Data/Document publishing date|
|---|ReportType|Provenance|Data origin and scope e.g. Modelled, Historic Event, Gauge Data, Tweet etc.|
|---|EventDate|Provenance|Date event occurred (if referring to an event)|
|---|Agent|Provenance|Organisation/Body type that published the work e.g. LLFA, Research Institution, Local Council|
|Object Property|hasReport|---|The object property linking a place to a document associated with the place (one way relationship). For example this could be the document that the data about the place originates from. As the provenance and other sub-classes as sub-classes of 'Report' then this property can be inherited by them too (Place can point directly to a 'Provenance' or other class instance/node)|
|Data Property|hasValue|---|The data property associated with all Provenance data and Provenance sub-classes. As the sub-classes are mixed in nature the data can take near any form, such as XSD.integer, XSD.string, xsd.decimal and so on.|

#### 'Place' classes
The place classes are NOT a location identifying property (e.g. postcode or coordinates) but instead are buildings, bridges, gauges or even land features. 
These classes are a way of identifying the data and what the data is referring to. For example, you may have a gauge station 
that has data associated with it. In our ontology context we would find a gauge node with some flow information associated with it.
Each flow instance would be a number associated with the gauge node.

|Reference Type|Label|Parent Class/Property|Description|
---|---|---|---
|Class|Place|---|The parent class of all objects of type place. Has object property references to other classes within the ontology and contains sub-classes of type place.|
|---|Property|---|One of many sub-classes of Place. This class is the superclass of all physical properties, whether it is a residential building, non-residential e.g. a shop or unclassified (currently used on the test database). An instance of this class can refer to a specific property or can refer to a collection of properties that share the same characteristic such as all being in the same location and having the same risk exposure.|
|---|Residential|||
|---|NonResidential|||
|---|Unclassified|||
|Object Property|hasReport|---|The same as 'hasReport' in the metadata classes table.|
|---|isAbout|---|The inverse property of the 'hasReport' class. This links a report type to a place type instance (one way relation as hasReport is the inverse.|
|Data Property|hasProperties|---||




