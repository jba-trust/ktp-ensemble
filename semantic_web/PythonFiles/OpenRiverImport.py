from rdflib import Graph, Literal, RDF, URIRef, BNode, Namespace
from rdflib.namespace import CSVW, DC, DCAT, DCTERMS, DOAP, FOAF, ODRL2, ORG, OWL, \
    PROF, PROV, RDF, RDFS, SDO, SH, SKOS, SOSA, SSN, TIME, \
    VOID, XMLNS, XSD
import pandas as pd
import geopandas as gpd
from JBANamespace import JBA, GEO, SF
from shapely.geometry import LineString
from shapely.wkt import loads


JBAGraph = Graph()
JBAGraph.bind("jba", JBA)
JBAGraph.bind("geo", GEO)

soa_shape_map_path = r"C:\Users\williamclayden\Documents\KTP\DevSpaceKendalDemo\ExampleFloodData\OSOpenRiverData\WatercourseLink.shp"
soa_shape_map = gpd.read_file(soa_shape_map_path)
soa_shape_map_geo = soa_shape_map.to_crs(epsg=4326)


riverdata = {'River': soa_shape_map_geo['name1'], 'length': soa_shape_map_geo['length'], 
             'geometry': soa_shape_map_geo['geometry'], 
             'fictitious':soa_shape_map_geo['fictitious']}

riversdf = gpd.GeoDataFrame(data=riverdata)
riversdf = riversdf[riversdf.fictitious=='false']

for row in range(0, riversdf.shape[0]):
    riverNode = BNode()
    blankgeom = BNode()
    JBAGraph.add((riverNode, RDF.type, JBA.River))
    JBAGraph.add((riverNode, GEO.hasGeometry, blankgeom))
    JBAGraph.add((blankgeom, RDF.type, JBA.RiverLinestring))
    if str(riversdf['River'][row]).lower() != "none":
        name = riversdf['River'][row]
        JBAGraph.add((riverNode, JBA.isCalled, Literal(str(riversdf['River'][row]), datatype=XSD.string)))
    elif str(riversdf['River'][row]).lower() == "none":
        JBAGraph.add((riverNode, JBA.isCalled, Literal("Tributary of "+str(name), datatype=XSD.string)))
    line3d_wkt = str(riversdf['geometry'][row])
    line_3d = loads(line3d_wkt)
    line_2d = LineString([xy[0:2] for xy in list(line_3d.coords)])
    JBAGraph.add((blankgeom, GEO.asWKT, Literal(line_2d.wkt, datatype=GEO.wktLiteral)))
    
    
JBAGraph.serialize(destination="C:\\Users\\williamclayden\\Documents\\KTP\\DevSpaceKendalDemo\\ExampleFloodData\\OSOpenRiversRDF.ttl",format='turtle')