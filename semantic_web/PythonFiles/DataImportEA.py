import re

import pandas as pd
from rdflib import Graph, Literal, BNode, Namespace
from rdflib.namespace import RDF, XSD

from JBANamespace import JBA, GEO

JBAGraph = Graph()
JBAGraph.bind("jba", JBA)
JBAGraph.bind("geo", GEO)
# Step 2: Insert single row to graph

PostcodeData = pd.read_csv(r"C:\Users\williamclayden\Documents\KTP\DevSpaceKendalDemo\ExampleFloodData\RoFRS_Postcode\RoFRS_KendalLA8LA9Cropped.csv", sep=",")
LatLongData = pd.read_csv(r"C:\Users\williamclayden\Documents\KTP\DevSpaceKendalDemo\ExampleFloodData\ukpostcodes\KendalPostcodeLongLat.csv")
LatLongData.rename(columns = LatLongData.iloc[0])
PostcodeData.rename(columns=PostcodeData.iloc[0])

# Create a blank risk node for each risk level. These describe the risk categories as detailed in the data documentation
lowRiskNode = BNode()
verylowRiskNode = BNode()
mediumRiskNode = BNode()
highRiskNode = BNode()
JBAGraph.add((lowRiskNode, RDF.type, JBA.LowRisk))
JBAGraph.add((verylowRiskNode, RDF.type, JBA.VeryLowRisk))
JBAGraph.add((mediumRiskNode, RDF.type, JBA.MediumRisk))
JBAGraph.add((highRiskNode, RDF.type, JBA.HighRisk))

# Add bounds to the risk nodes based on the document (ideally this would be via)
JBAGraph.add((verylowRiskNode, JBA.riskUpperBound,Literal(int(1000), datatype=XSD.nonNegativeInteger)))
JBAGraph.add((verylowRiskNode, JBA.riskDefinition, Literal("Very Low: each year, there is a chance of flooding of less than 1 in 1000 (0.1%).", datatype=XSD.string)))
JBAGraph.add((lowRiskNode, JBA.riskUpperBound,Literal(int(100), datatype=XSD.nonNegativeInteger)))
JBAGraph.add((lowRiskNode, JBA.riskLowerBound,Literal(int(1000), datatype=XSD.nonNegativeInteger)))
JBAGraph.add((lowRiskNode, JBA.riskDefinition, Literal("Low: each year, there is a chance of flooding of between 1 in 100 (1%) and 1 in 1000 (0.1%).", datatype=XSD.string)))
JBAGraph.add((mediumRiskNode, JBA.riskUpperBound,Literal(int(30), datatype=XSD.nonNegativeInteger)))
JBAGraph.add((mediumRiskNode, JBA.riskLowerBound,Literal(int(100), datatype=XSD.nonNegativeInteger)))
JBAGraph.add((mediumRiskNode, JBA.riskDefinition, Literal("Medium: each year, there is a chance of flooding of between 1 in 30 (3.3%) and 1 in 100 (1%).", datatype=XSD.string)))
JBAGraph.add((highRiskNode, JBA.riskLowerBound,Literal(int(30), datatype=XSD.nonNegativeInteger)))
JBAGraph.add((highRiskNode, JBA.riskUpperBound, Literal(int(0),datatype=XSD.nonNegativeInteger)))
JBAGraph.add((highRiskNode, JBA.riskDefinition, Literal("High: each year, there is a chance of flooding of greater than 1 in 30 (3.3%).", datatype=XSD.string)))


# Create the document node
docnode = BNode()
JBAGraph.add((docnode, RDF.type, JBA.Document))
JBAGraph.add((docnode, JBA.documentName, Literal("EARiskofFloodingfRaSPostcodesinRiskAreas", datatype=XSD.string)))
# Creating a series of provenance type nodes
authorNode = BNode()
agentNode = BNode()
pdateNode = BNode()
rtypeNode = BNode()
descNode = BNode()
JBAGraph.add((docnode, JBA.hasProvenance, authorNode))
JBAGraph.add((docnode, JBA.hasProvenance, agentNode))
JBAGraph.add((docnode, JBA.hasProvenance, pdateNode))
JBAGraph.add((docnode, JBA.hasProvenance, rtypeNode))
JBAGraph.add((docnode, JBA.hasProvenance, descNode))
JBAGraph.add((authorNode, RDF.type, JBA.Author))
JBAGraph.add((agentNode, RDF.type, JBA.Agent))
JBAGraph.add((pdateNode, RDF.type, JBA.PublishDate))
JBAGraph.add((rtypeNode, RDF.type, JBA.ReportType))
JBAGraph.add((descNode, RDF.type, JBA.Description))

JBAGraph.add((authorNode, JBA.hasValue, Literal(" EA, Environment Agency", datatype=XSD.string)))
JBAGraph.add((agentNode, JBA.hasValue, Literal(" Government Organisation", datatype=XSD.string)))
JBAGraph.add((pdateNode, JBA.hasValue, Literal("2018-03-01", datatype=XSD.date)))
JBAGraph.add((rtypeNode, JBA.hasValue, Literal(" Risk Model, Dataset", datatype=XSD.string)))
JBAGraph.add((descNode, JBA.hasValue, Literal("An assessment showing the risk of flooding from rivers and sea for England, taking into account the presence and condition of flood defences. It is produced using local data and expertise.", datatype=XSD.string)))

#The Building node is no longer required as it exists in the ontology. Each individual property node can be connected to the other nodes by virtue of being subclass of property
prop_list = ["RES_CNT_", "NRP_CNT_","UNC_CNT_"]
for row in range(0, PostcodeData.shape[0]):
    blankGeom = BNode()
    placeNode = BNode() # Town/village name
    JBAGraph.add((blankGeom, RDF.type, GEO.Point))
    # Format is longitude then latitude
    JBAGraph.add((blankGeom,GEO.asWKT,Literal("POINT ("+str(LatLongData['long'][row])+" "+str(LatLongData['lat'][row])+")", datatype=GEO.wktLiteral)))
    if LatLongData['place'][row] == "Kendal": # For now we only classify as town or village, not ideal but demonstrates the case
        JBAGraph.add((placeNode, RDF.type, JBA.Town))
        JBAGraph.add((placeNode, JBA.isCalled, Literal(str(LatLongData['place'][row]), datatype=XSD.string)))
        JBAGraph.add((placeNode, JBA.hasDocument, docnode))
    else:
        JBAGraph.add((placeNode, RDF.type, JBA.Village))
        JBAGraph.add((placeNode, JBA.isCalled, Literal(str(LatLongData['place'][row]), datatype=XSD.string)))
        JBAGraph.add((placeNode, JBA.hasDocument, docnode))
    for column in range(0,len(PostcodeData.columns)-5):
        if column % 3 == 0:
            resNode = BNode()
            JBAGraph.add((resNode, RDF.type, JBA.Residential)) # Create a property type associated with the res node
            JBAGraph.add((resNode,GEO.hasGeometry,blankGeom))
            JBAGraph.add((resNode, JBA.hasDocument, docnode))
            JBAGraph.add((resNode, JBA.hasPostcode, Literal(PostcodeData['PC'][row],datatype=XSD.string))) # Add a postcode value from the data
            JBAGraph.add((resNode, JBA.hasDistrictcode, Literal(re.search(r'([^\s]+)',PostcodeData['PC'][row]).group(1), datatype=XSD.string)))
            JBAGraph.add((resNode, JBA.isIn, placeNode))
            prop_ID = 0
        elif column % 3 == 1:
            nonresNode = BNode()
            JBAGraph.add((nonresNode, RDF.type, JBA.NonResidential)) # Create a property type associated with the property node
            JBAGraph.add((nonresNode,GEO.hasGeometry,blankGeom))
            JBAGraph.add((nonresNode, JBA.hasDocument, docnode))
            JBAGraph.add((nonresNode, JBA.hasPostcode, Literal(PostcodeData['PC'][row],datatype=XSD.string))) # Add a postcode value from the data
            JBAGraph.add((nonresNode, JBA.hasDistrictcode, Literal(re.search(r'([^\s]+)',PostcodeData['PC'][row]).group(1), datatype=XSD.string)))
            JBAGraph.add((nonresNode, JBA.isIn, placeNode))
            prop_ID = 1
        elif column % 3 == 2:
            unknownNode = BNode() # For unknown property types
            JBAGraph.add((unknownNode, RDF.type, JBA.Unknown)) # Create a property type associated with the property node
            JBAGraph.add((unknownNode,GEO.hasGeometry,blankGeom))
            JBAGraph.add((unknownNode, JBA.hasDocument, docnode))
            JBAGraph.add((unknownNode, JBA.hasPostcode, Literal(PostcodeData['PC'][row],datatype=XSD.string))) # Add a postcode value from the data
            JBAGraph.add((unknownNode, JBA.hasDistrictcode, Literal(re.search(r'([^\s]+)',PostcodeData['PC'][row]).group(1), datatype=XSD.string)))
            JBAGraph.add((unknownNode, JBA.isIn, placeNode))
            prop_ID = 2
        else: # Added 'exception' detection, temporary as not robust
            print("Broken code")
            break
        if column < 3:
            if column % 3 == 0:
                JBAGraph.add((resNode, JBA.hasRisk, verylowRiskNode)) # Create risk associated with the given property
                JBAGraph.add((resNode, JBA.hasCount, Literal(int(PostcodeData[prop_list[prop_ID] + 'VeryLow'][row]), datatype=XSD.integer)))
            if column % 3 == 1:
                JBAGraph.add((nonresNode, JBA.hasRisk, verylowRiskNode)) # Create risk associated with the given property
                JBAGraph.add((nonresNode, JBA.hasCount, Literal(int(PostcodeData[prop_list[prop_ID] + 'VeryLow'][row]), datatype=XSD.integer)))
            if column % 3 == 2:
                JBAGraph.add((unknownNode, JBA.hasRisk, verylowRiskNode)) # Create risk associated with the given property
                JBAGraph.add((unknownNode, JBA.hasCount, Literal(int(PostcodeData[prop_list[prop_ID] + 'VeryLow'][row]), datatype=XSD.integer)))

        elif column < 6:
            if column % 3 == 0:
                JBAGraph.add((resNode, JBA.hasRisk, lowRiskNode)) # Create risk associated with the given property
                JBAGraph.add((resNode, JBA.hasCount, Literal(int(PostcodeData[prop_list[prop_ID] + 'Low'][row]), datatype=XSD.integer)))
            if column % 3 == 1:
                JBAGraph.add((nonresNode, JBA.hasRisk, lowRiskNode)) # Create risk associated with the given property
                JBAGraph.add((nonresNode, JBA.hasCount, Literal(int(PostcodeData[prop_list[prop_ID] + 'Low'][row]), datatype=XSD.integer)))
            if column % 3 == 2:
                JBAGraph.add((unknownNode, JBA.hasRisk, lowRiskNode)) # Create risk associated with the given property
                JBAGraph.add((unknownNode, JBA.hasCount, Literal(int(PostcodeData[prop_list[prop_ID] + 'Low'][row]), datatype=XSD.integer)))
        elif column < 9:
            if column % 3 == 0:
                JBAGraph.add((resNode, JBA.hasRisk, mediumRiskNode)) # Create risk associated with the given property
                JBAGraph.add((resNode, JBA.hasCount, Literal(int(PostcodeData[prop_list[prop_ID] + 'Medium'][row]), datatype=XSD.integer)))
            if column % 3 == 1:
                JBAGraph.add((nonresNode, JBA.hasRisk, mediumRiskNode)) # Create risk associated with the given property
                JBAGraph.add((nonresNode, JBA.hasCount, Literal(int(PostcodeData[prop_list[prop_ID] + 'Medium'][row]), datatype=XSD.integer)))
            if column % 3 == 2:
                JBAGraph.add((unknownNode, JBA.hasRisk, mediumRiskNode)) # Create risk associated with the given property
                JBAGraph.add((unknownNode, JBA.hasCount, Literal(int(PostcodeData[prop_list[prop_ID] + 'Medium'][row]), datatype=XSD.integer)))
        elif column < 12:
            if column % 3 == 0:
                JBAGraph.add((resNode, JBA.hasRisk, highRiskNode)) # Create risk associated with the given property
                JBAGraph.add((resNode, JBA.hasCount, Literal(int(PostcodeData[prop_list[prop_ID] + 'High'][row]), datatype=XSD.integer)))
            if column % 3 == 1:
                JBAGraph.add((nonresNode, JBA.hasRisk, highRiskNode)) # Create risk associated with the given property
                JBAGraph.add((nonresNode, JBA.hasCount, Literal(int(PostcodeData[prop_list[prop_ID] + 'High'][row]), datatype=XSD.integer)))
            if column % 3 == 2:
                JBAGraph.add((unknownNode, JBA.hasRisk, highRiskNode)) # Create risk associated with the given property
                JBAGraph.add((unknownNode, JBA.hasCount, Literal(int(PostcodeData[prop_list[prop_ID] + 'High'][row]), datatype=XSD.integer)))

JBAGraph.serialize(destination="C:\\Users\\williamclayden\\Documents\\KTP\\DevSpaceKendalDemo\\ExampleFloodData\\EADatawDocumentName.ttl",format='turtle')
