from rdflib import Graph, Literal, BNode, Namespace
from rdflib.namespace import RDF, XSD

# Step 1: generate Graph and JBA namespace
JBA = Namespace("https://www.jbaconsulting.com/ontology/")
GEO = Namespace("http://www.opengis.net/ont/geosparql#")

# Classes
JBA.Flood
JBA.NaturalHazard
JBA.Provenance
JBA.Agent
JBA.Author
JBA.PublishDate
JBA.ReportType
JBA.Location
JBA.Document
JBA.Risk
JBA.VeryLowRisk
JBA.LowRisk
JBA.MediumRisk
JBA.HighRisk
JBA.Place
JBA.Property
JBA.Unknown
JBA.NonResidential
JBA.Residential
JBA.Description
JBA.Tags
JBA.Neighbourhood
JBA.AdministrativeArea
JBA.City
JBA.Town
JBA.Village
JBA.Cause

#Object Properties
JBA.hasHazard
JBA.hasRisk
JBA.hasLocation
JBA.isAbout
JBA.hasDocument

#Data properties
JBA.latitude
JBA.longitude
JBA.geometry # Point(long lat)
JBA.riskUpperBound
JBA.riskLowerBound
JBA.hasProperties
JBA.hasDistrictcode
JBA.hasAddress
JBA.hasPostcode
JBA.hasValue
JBA.riskDefinition
JBA.isCalled
JBA.hasProvenance
JBA.isIn
JBA.documentName

# Geo binds
GEO.asWKT
GEO.hasPointGeometry

#Data types
GEO.wktLiteral

JBAGraph = Graph()
JBAGraph.bind("jba", JBA)
JBAGraph.bind("geo", GEO)

# Create blank nodes for all the provenance/document information
AuthorNode = BNode()
AgentNode = BNode()
DescNode = BNode()
EDateNode = BNode()
PDateNode = BNode()
RTypeNode = BNode()
DocNode = BNode()

JBAGraph.add((DocNode, RDF.type, JBA.Document))
JBAGraph.add((DocNode, JBA.documentName, Literal("KendalFIR05122015", datatype=XSD.string))) # Could just use doc name so long as its unique
JBAGraph.add((DocNode, JBA.hasProvenance, AuthorNode))
JBAGraph.add((DocNode, JBA.hasProvenance, AgentNode))
JBAGraph.add((DocNode, JBA.hasProvenance, DescNode))
JBAGraph.add((DocNode, JBA.hasProvenance, EDateNode))
JBAGraph.add((DocNode, JBA.hasProvenance, PDateNode))
JBAGraph.add((DocNode, JBA.hasProvenance, RTypeNode))
JBAGraph.add((AuthorNode,RDF.type,JBA.Author))
JBAGraph.add((AuthorNode, JBA.hasValue, Literal(" EA, Environment Agency, Cumbria County Council, David Webborn", datatype=XSD.string)))
JBAGraph.add((AgentNode, RDF.type,JBA.Agent))
JBAGraph.add((AgentNode,RDF.type,Literal(" Non-departmental public body, Government Agency, Non-metropolitan county council, Lead Local Flood Authority, LLFA", datatype=XSD.string)))
JBAGraph.add((DescNode, RDF.type,JBA.Description))
JBAGraph.add((DescNode, JBA.hasValue, Literal("The flooding experienced in Kendal on the 5th and 6th of December 2015 was the largest flood event ever \
                recorded in the town, and was the result of the effects of Storm Desmond. This storm caused a period of \
              prolonged, intense rainfall across Northern England. This rainfall fell on catchments that were already \
              saturated and resulted in high river levels and flooding throughout Cumbria and further afield. The flow \
              in the River Kent and its tributaries through Kendal on the 5 \
              th of December was the highest ever recorded \
              and the various flooding mechanisms that occurred caused widespread flooding throughout the town.", datatype=XSD.string)))
JBAGraph.add((EDateNode, RDF.type, JBA.EventDate))

# XSD datetime format: YYYY-MM-DDThh:mm:ss
JBAGraph.add((EDateNode, JBA.hasValue, Literal("2015-12-05", datatype=XSD.date)))
JBAGraph.add((EDateNode, JBA.hasValue, Literal("2015-12-06", datatype=XSD.date)))
JBAGraph.add((PDateNode, RDF.type, JBA.PublishDate))
JBAGraph.add((EDateNode, JBA.hasValue, Literal("2016-12-20", datatype=XSD.date)))
JBAGraph.add((RTypeNode, RDF.type, JBA.ReportType))
JBAGraph.add((RTypeNode, JBA.hasValue, Literal("Section-19", datatype=XSD.string)))

# Create the appropriate nodes for the location/place associations

KendalNode = BNode()
SLandNode = BNode()
MinFNode = BNode()
JBAGraph.add((KendalNode, RDF.type, JBA.Town))
JBAGraph.add((KendalNode, JBA.isCalled, Literal("Kendal", datatype=XSD.string)))
JBAGraph.add((SLandNode, RDF.type, JBA.Neighbourhood))
JBAGraph.add((SLandNode, JBA.isCalled, Literal("Sandylands", datatype=XSD.string)))
JBAGraph.add((MinFNode, RDF.type, JBA.Neighbourhood))
JBAGraph.add((MinFNode, JBA.isCalled, Literal("Mintsfeet", datatype=XSD.string)))
JBAGraph.add((MinFNode, JBA.isIn, KendalNode))
JBAGraph.add((SLandNode, JBA.isIn, KendalNode))
JBAGraph.add((KendalNode, JBA.hasDocument, DocNode))
JBAGraph.add((SLandNode, JBA.hasDocument, DocNode))
JBAGraph.add((MinFNode, JBA.hasDocument, DocNode))

JBAGraph.serialize(destination="C:\\Users\\williamclayden\\Documents\\KTP\\DevSpaceKendalDemo\\ExampleFloodData\\KendalS19Demo.ttl",format='turtle')





