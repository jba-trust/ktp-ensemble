# -*- coding: utf-8 -*-
"""
Created on Tue May  4 09:59:55 2021

@author: WilliamClayden
"""
from rdflib import Graph, Literal, Namespace


JBA = Namespace("https://www.jbaconsulting.com/ontology/")
GEO = Namespace("http://www.opengis.net/ont/geosparql#")
SF = Namespace("http://www.opengis.net/ont/sf#")

# Classes
JBA.Document

# Landuse classes
JBA.LandUse
JBA.Built
JBA.DefenceInfrastructure
JBA.ReceptorInfrastructure
JBA.HistoricLandmarks
JBA.Building
JBA.NonResidential
JBA.Residential
JBA.Unknown
JBA.Natural
JBA.Field
#HazardClasses
JBA.NaturalHazard
JBA.Flood
#Place classes
JBA.Place
JBA.AdministrativeArea
JBA.Country
JBA.County
JBA.Settlement
JBA.Hamlet
JBA.City
JBA.Neighbourhood
JBA.Town
JBA.Village
#Provenance classes
JBA.Provenance
#Risk classes (Temporary will be updated soon)
JBA.Risk
JBA.LongTermRisk
JBA.MediumTermRisk
JBA.ShortTermRisk
JBA.RiskLevel
JBA.HighRisk
JBA.LowRisk
JBA.MediumRisk
JBA.VeryHighRisk
JBA.VeryLowRisk
#Watercourse classes
JBA.Watercourse
JBA.Lake
JBA.River
JBA.LargeRiver
JBA.MediumRiver
JBA.SmallRiver
JBA.Stream
# Geometry classes
JBA.RiverLinestring


SF.Point


#Object Properties
JBA.hasHazard
JBA.hasInformationIn
JBA.hasNeighbourhood
JBA.hasProvenance
JBA.hasRisk
JBA.isAbout
JBA.isIn
JBA.neighbourhoodIn

#Data properties
JBA.hasLocation
JBA.hasDistrictcode
JBA.hasAddress
JBA.hasPostcode
JBA.hasAddress
JBA.latitude
JBA.longitude
JBA.documentName
JBA.hasAttributes
JBA.hasArea
JBA.hasCount
JBA.hasResidents
JBA.propertyAge
JBA.propertyName
JBA.propertyValue
JBA.provenanceInfo
JBA.hasDocumentType
JBA.hasAuthor
JBA.hasAgent
JBA.hasPublishDate
JBA.tags
JBA.hasDescription
JBA.hasEventDate

# Geo binds
GEO.asWKT
GEO.hasPointGeometry
GEO.hasGeometry # A wkt literal type geometry


#Data types
GEO.wktLiteral