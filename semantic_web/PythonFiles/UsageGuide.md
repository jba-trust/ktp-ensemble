# Usage Guide
This document is written with the intention of being a guide for users importing data 
to the database. This is written to pair with the Ontology guide to inform how data should
be manipulated for graph insertion and any best practices to make the most of the software.

### Side notes about Graph Databases

As you can see from this document, Graph Databases do not conform to the standard tabular
form that most data is collected in (e.g. csv) and also stored in (e.g. RDBMS). On the
surface this is an issue because a graph database requires significant data transformation
before 