from urllib.parse import quote
import speech_recognition as sr
import time
import requests
import json
import pandas as pd
from io import StringIO

class DatabaseConnection:
    def __init__(self, database_url):
        self.repo = database_url
        self.status = False  # Database connection (only allow requests if valid connection)
        self.__test_connection()

    def __test_connection(self):
        #Test query for response (there has to be a more natural way to do this)
        response = requests.get(self.repo, headers=self.__format_selector('csv'), params={'query': 'PREFIX jba: <https://jbaconsulting.com/ontology/> SELECT ?area WHERE {?area jba:PostalCode ?addressNode.}', 'charset':'utf-8'})
        if response.status_code == 200:
            print("Status code 200: Database connection success")
        elif response.status_code != 200:
            print("Status code " + str(response.status_code) + ": Database connection failed. See html status codes")
            print("or test that the database is on")
            self.status = False

    #TODO: Add more options for the format type (customisation should be unnecessary but optional anyway)
    @staticmethod
    def __format_selector(format):
        if format.lower() == 'csv':
            return {'Accept':'text/csv'}
        elif format.lower() == 'html':
            return {'Accept':'text/html'}
        elif format.lower() == 'json':
            return {'Accept':'application/json'}
        else:
            print("Invalid format, returning query as json")
            return {'Accept':'application/json'}

    @staticmethod
    def __content_type_select(type):
        if type.lower() == 'csv':
            return {'Content-Type':'text/csv'}
        elif type.lower() == 'html':
            return {'Accept':'text/html'}
        elif type.lower() == 'json':
            return {'Accept':'application/json'}
        else:
            print("Invalid type, returning query as json")
            return {'Accept':'text/json'}

    def query_request(self, query_string, format='json'): # Process and format query
        return requests.get(self.repo, headers=self.__format_selector(format), params={'query': query_string, 'charset':'utf-8'}).text

    #TODO: Finish the data insertion method
    def import_to_repo(self,type):
        requests.post(self.repo, headers=self.__content_type_select(type))
        return

# If name is main, create a database connection instance and test the connection
if __name__ == "main":
    test = DatabaseConnection('http://localhost:7200/repositories/KendalPrototype')

#TODO: Use inference and Source pathway receptor model to evaluate the risk as how much impact the assesment
# to how the data is reported, what are the impacts, the infrastructure risk. Analyse impact across how
