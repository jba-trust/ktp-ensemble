from rdflib import Graph, Literal, Namespace


JBA = Namespace("https://www.jbaconsulting.com/ontology/")
GEO = Namespace("http://www.opengis.net/ont/geosparql#")
SF = Namespace("http://www.opengis.net/ont/sf#")

# Classes
JBA.Document
