import glob
import os
import rdflib
from rdflib import Graph, Literal, BNode, Namespace, URIRef
from rdflib.extras.describer import Describer
import datetime
import shutil
from rdflib.namespace import RDF, XSD
#TODO: Add logging
#TODO: Create a document parsing class
class DocumentParser:
    def __init__(self, folder_path):
        self.folder_path = folder_path
        self.missing_tag_count = [] # A list of the tag files and how many tags are missing
        self.average_missing_tag_num = 0 # An average number of how many tags are missing from the documents in the directory
        self.__taglist_existence()
        self.rdf_converter = RDFConverter()

    def __taglist_existence(self): # Makes sure that any document is paired with a corresponding text file
        folderlist = [f for f in os.walk(self.folder_path)]
        tagfolders_made = 0
        for folder in folderlist:
            if (not folder[0].endswith("\\Tags") and not ("\\Tags" or "\\RDFFormatted" in folder[1])) and (len(folder[2]) > 0):# Create a Tags subfolder for the document tags text files
                try:
                    os.mkdir(folder[0] + "/Tags")
                except OSError:
                    print("Creation of the tag directory %s failed" % self.folder_path + "/Tags")
                else:
                    tagfolders_made+=1
                with open(os.getcwd()+"\\TagFormat.txt", "r") as tagdoc_template:
                    tagdoc_format ="".join(tagdoc_template.readlines())
                for file in folder[2]:
                    try:
                        tagfile = open(folder[0]+"/Tags/"+file.split(".",-1)[0] + "tags.txt", "w")
                        tagfile.write(tagdoc_format)
                        tagfile.close()
                    except IOError:
                        print("File read'/write error")
            if (not folder[0].endswith("\\RDFFormatted") and not ("\\RDFFormatted" in folder[1])) and not folder[0].endswith("\\Tags") and (len(folder[2]) > 0):# Create a Tags subfolder for the document tags text files
                try:
                    os.mkdir(folder[0] + "/RDFFormatted")
                except OSError:
                    print("Creation of the tag directory %s failed" % self.folder_path + "/Tags")

    #TODO: Allow existing tags to be edited
    def tag_doc_update(self):
        return

    #TODO: Allow tags to be entered manually and automatically (nlp) to documents
    def tag_doc_populate(self):
        folderlist = [f for f in os.walk(self.folder_path)]
        for folder in folderlist:
            if ("Tags" in folder[1]):
                tag_files = os.listdir(folder[0]+"\\Tags")
                for i in range(len(tag_files)):
                    current_file = tag_files[i]
                    with open(folder[0] + "/Tags/"+current_file, "r+") as tag_doc:
                        #TODO: Convert to list comphrensions if possible
                        tags_list =[]
                        for tag in tag_doc:
                            if tag.__contains__("--------") and tag.split(":", 1)[0] not in ['tagdate', 'documentformat']:
                                print("Insert tags to do with " + tag.split(":", 1)[0] + " in document %s" % current_file.split(".txt", 1)[0][:-4])
                                print("Separate all tags with a comma or a space")
                                #TODO: Recognise words from a dictionary and use them instead to standardise/remove errors
                                #TODO: Change spaces to commas
                                tags_list.append(tag.replace("--------", input()))
                            elif tag.__contains__("--------"): #TODO: Check the tags are split appropriately
                                if tag.split(":", 1)[0] == 'tagdate':
                                    tags_list.append(tag.replace("--------", datetime.datetime.now().strftime("%d %m %Y").replace(" ", "")))
                                elif tag.split(":", 1)[0] == 'documentformat': # Match the tag document to the original document and find the file type
                                    tags_list.append(tag.replace("--------", [match for match in folder[2]  if current_file.strip("tags.txt") in match][0].split(".")[-1]))
                        tag_doc.seek(0)
                        tag_doc.write("".join(tags_list))
                        tag_doc.truncate()
                        tag_doc.close()

    #Look at all the tag documents and log which ones have missing entries/how many are missing (not specific)
    def tag_checker(self):
        self.missing_tag_count = [] #TODO: Update missing tag counter
        folderlist = [f for f in os.walk(self.folder_path)]
        self.average_missing_tag_num = 0
        tag_file_count = 0
        for folder in folderlist:
            if ("Tags" in folder[1]):
                tag_files = os.listdir(folder[0]+"\\Tags")
                for i in range(len(tag_files)):
                    current_file = tag_files[i]
                    with open(folder[0] + "/Tags/"+current_file) as tag_list:
                        tags_missing = sum([True for tag in tag_list if tag.__contains__("--------")])
                        self.missing_tag_count.append([folder[0] + "/Tags/"+current_file, tags_missing])
                        self.average_missing_tag_num += int(tags_missing)
        self.average_missing_tag_num = self.average_missing_tag_num/len(self.missing_tag_count)

    def rdf_conversion(self):
        doclist = [f for f in os.walk(self.folder_path)]
        for i in range(0, len(doclist)): #TODO: See if there is a better iteration method here
        #TODO: Omit all documents in /Tags subfolders when counting
            if not (doclist[i][0].endswith("\\Tags") or (doclist[i][0].endswith("\\RDFFormatted"))):
                for j in range(int(len(doclist[i][2]))):
                    document_path = doclist[i][0] + "/"
                    document_name = doclist[i][2][j]
                    #TODO: Figure out the best way to have uniqueness in data
                    doc = URIRef("https://jbaconsulting.co.uk/ontologies/documentstore/documents/"+str(document_name.split(".", 1)[0]))
                    with open(str(document_path)+"Tags/"+document_name.split(".", -1)[0] +"tags.txt", "r") as tagdoc:
                        tags=[string.strip("\n") for string in tagdoc.readlines()]
                        self.rdf_converter.doc_as_rdf(document_path, document_name,tags) #TODO: Complete the tag generator method
                    tagdoc.close()
        return

class RDFConverter:
    def __init__(self):
        #TODO: Initialise the graph and import the ontology
        #TODO: Make functions to process each tag type for the document
        self.methodlist = {"tagdate": "date_handler", "author":"publishing_handler","places": "place_handler", "publishingorganisations": "publishing_handler",
                           "documenttype": "document_handler", "documentformat": "document_handler", "eventdate":"date_handler",
                           "eventduration":"date_handler","publishdate":"date_handler","dataorigin":"description_handler",
                           "scope":"description_handler","geographicfeatures":"description_handler", "landuse": "description_handler",
                           "modelled":"description_handler", "POI":"place_handler","population":"place_handler", "description":
                           "description_handler","source":"description_handler", "pathway":"description_handler", "receptor":"description_handler"}

    def date_handler(self,graph,tag_name,tag_items, document_describer): # Handles tagdate, event date, duration and publishing date
        DOC = rdflib.Namespace('https://www.jbaconsulting.co.uk/ontologies/documentstore/')
        if tag_name == 'tagdate':
            #TODO: Recognise the time format inserted and edit as appropriate
            document_describer.value(DOC.tagdate, datetime.date(int(tag_items[0][4:]),int(tag_items[0][2:4]),int(tag_items[0][0:2])))
        elif tag_name == "eventdate":
            for eventdate in tag_items:
                document_describer.value(DOC.eventdate, datetime.date(int(eventdate[4:]), int(eventdate[2:4]), int(eventdate[0:2])))
        elif tag_name =='eventduration':
            document_describer.value(DOC.eventduration, int(tag_items[0]))
        elif tag_name =='publishdate':
            document_describer.value(DOC.publishdate, datetime.date(int(tag_items[0][4:]), int(tag_items[0][2:4]), int(tag_items[0][0:2])))
        return graph, document_describer

    def place_handler(self,graph,tag_name,tag_items, document_describer): # Handles tags about the place
        DOC = rdflib.Namespace('https://www.jbaconsulting.co.uk/ontologies/documentstore/')
        if tag_name == 'places':
            for place in tag_items:
                #TODO: Change DOC place to a better Ontology place reference e.g. DBpedia
                place_node = URIRef("https://www.jbaconsulting.co.uk/ontologies/documentstore/Places/"+place.replace(" ", ""))
                document_describer.rel(DOC.isAbout, place_node)
        elif tag_name == 'population':
            for pop in tag_items:
                pop = pop.split(".",-1)
                if len(pop) >1:
                    place_node = URIRef("https://www.jbaconsulting.co.uk/ontologies/documentstore/Places/"+pop[0].replace(" ", ""))
                    graph.add((place_node, DOC.population, Literal(int(pop[1]), datatype=XSD.NonNegativeInteger)))
        elif tag_name == 'POI':
            document_describer.value(DOC.poi, tag_items[0])
        return graph, document_describer

    def publishing_handler(self,graph,tag_name,tag_items, document_describer):
        DOC = rdflib.Namespace('https://www.jbaconsulting.co.uk/ontologies/documentstore/')
        if tag_name == "publishingorganisation":
            for org in tag_items:
                document_describer.value(DOC.publishedby, org)
        elif tag_name =='author':
            for author in tag_items:
                document_describer.value(DOC.authoredby, author)
        return graph, document_describer

    def description_handler(self,graph,tag_name,tag_items, document_describer):
        DOC = rdflib.Namespace('https://www.jbaconsulting.co.uk/ontologies/documentstore/')
        if tag_name =='scope':
            for item in tag_items:
                document_describer.value(DOC.scope, item)
        elif tag_name =='modelled':
            document_describer.value(DOC.modelled, tag_items[0])
        elif tag_name == 'dataorigin':
            for origin in tag_items:
                document_describer.value(DOC.dataorigin,origin)
        elif tag_name == 'source':
            for source in tag_items:
                document_describer.value(DOC.source, source)
        elif tag_name == 'landuse':
            for landuse in tag_items:
                document_describer.value(DOC.landuse, landuse)
        elif tag_name == 'description':
            for detail in tag_items:
                document_describer.value(DOC.description, detail)
        elif tag_name =='receptor':
            for receptor in tag_items:
                document_describer.value(DOC.receptor, receptor)
        elif tag_name == 'pathway':
            for pathway in tag_items:
                document_describer.value(DOC.pathway, pathway)
        return graph, document_describer

    def document_handler(self,graph,tag_name,tag_items, document_describer):
        DOC = rdflib.Namespace('https://www.jbaconsulting.co.uk/ontologies/documentstore/')
        if tag_name == 'documenttype':
            for tag in tag_items:
                document_describer.value(DOC.documenttype, tag_items[0])
        elif tag_name == 'documentformat':
            document_describer.value(DOC.documentformat, tag_items[0])
        return graph, document_describer

    def doc_as_rdf(self, file_location, document_name,tags): #Creates rdf descriptions of the document and then saves each as an individual file (rdf file subfolder)
        docGraph = rdflib.Graph()
        #docGraph.parse(format='application/rdf+xml',source=r'C:\Users\williamclayden\Documents\KTP\DevSpaceKendalDemo\Notes and Versions\OntologyVersions\JBADocumentStoreOntology140621.owl')
        DOC = rdflib.Namespace('https://www.jbaconsulting.co.uk/ontologies/documentstore/')
        docGraph.bind('jbadoc', DOC)
        document = Describer(docGraph)
        document.about('https://www.jbaconsulting.co.uk/ontologies/documentstore/Document/'+str(document_name)+'#document')
        document.rdftype(DOC.Document)
        for tag in tags:
            tag_name , tag_items = tag.split(":", -1)[0], tag.split(":", -1)[1].strip(";").split(",",-1)
            docGraph, document = getattr(self, self.methodlist.get(tag_name))(docGraph,tag_name, tag_items, document)
        docGraph.serialize(destination=str(file_location)+"RDFFormatted/"+document_name.split(".",-1)[0]+"rdf.ttl",format='turtle')



def get_bool(prompt):
    while True:
        try:
            return {"true":True,"false":False, "1":True, "0":False}[str(prompt)]
        except KeyError:
            print("Invalid input please enter True or False (1 or 0)")
            prompt = str(input())

#TODO: automate/semi-automate this classification/feed back from NLP
#TODO: Search all subfolders to find the needed documents and their tags when auto
#TODO: Move method to a class, include a method that automatically creates tagging files for documents that dont have one
#TODO: Detect when tagging files are incomplete and suggest filling them out.
#TODO: Add logging if beneficial
if __name__ == "__main__":
    #TODO: let the user input the folder location OR let them specify a file and handle just that specific document
    doc_to_rdf = DocumentParser(r"C:/Users/williamclayden/Documents/KTP/DevSpaceKendalDemo/FolderStructureExample/")
    doc_to_rdf.tag_checker()
    print("There are on average %s " % doc_to_rdf.average_missing_tag_num +"tags missing per document in this location")
    if get_bool(str(input("Enter '1' if you want to add tags to the documents in your directory or '0' to continue \n"))):
        doc_to_rdf.tag_doc_populate()
    if get_bool(str(input("Enter '1' if you want to convert the document references and their tags in your directory to rdf form or '0' to continue \n"))):
        doc_to_rdf.rdf_conversion()
    #TODO: replace the fixed directory with a manual input