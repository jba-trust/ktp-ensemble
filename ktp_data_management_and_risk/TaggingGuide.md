# Document Tagging guide

Currently, any document can be tagged either via the "tags" where available or where not available you can use a text
document with the format and fields as described below. Each grouping of tags contains either
a single label or a list of labels. These labels should be terms you would expect in a semantic 
search. I.e. The type of place instead of the place itself. This distinction was made so that
a town such as Skipton is associated with the place and the geo-spatial references rather than
a linguistic description.

A clear distinction is made between tags which are treated as class nodes (more intuitive for similarity)
and the document descriptions such as file location and description which are both datatype instances.

##Formatting and Tagging Types

The document name format should be in an organisation (lead authority that published the work),
document title then publish date (preferably in ISO 8601). E.g. CumbriaCountyCouncilFloodReport20160110
#TODO: This may change as a better naming convention is adopted for including historic files

The tagging template is to be auto-generated when a document is added to the subfolder and the tag creation script is run.

tagdate:-------; places:-------; publishingorganisations:-------; author:--------; documenttype:--------; eventdate:-------;
eventduration:-------; publishdate:-------; dataorigin:-------; scope:-------; geographicfeatures:-------; landuse:-------;
modelling:-------; POI:--------; population:-------; description:-------;
###TagDate
The tag date is the date which tags were most recently updated for a given document/source. This
is useful to identify when and how the tags were generated.
The format is given in ISO 8601 format (yyyymmdd). E.g. TagDate:20210601;

###Scope
Scope of a document in this instance are labels concerning the area descriptors the documents concern, and
the topics the document describes. The scope of a document can appear as a single label or as a 
list of labels. Different labels are separated by a comma. 
E.g. Scope:Postcode,National,Insurance;

Similar to http://purl.org/dc/elements/1.1/coverage

####Area descriptors
Country, County


###ExpectedPeriod
The expected period is the time frame the event is expected to occur over.

###Modelled

###Data Origin


###POI
The POI is important in this instance as it refers to the specific place this associated to instead of
similar associations