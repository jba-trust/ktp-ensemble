# Flood Data Extraction

This tool is for extracting data from ["natural text"](https://en.wikipedia.org/wiki/Natural_language). 
It works on PDFs and plain text files. This process involves two steps: 
Firstly the tool needs to be trained on the data you're interested in, 
and then the tool can generalise and extract from other (similar) files.

This document outlines installation instructions, as well as an in-depth breakdown of the process of using the tool.
Provided with this tool are some [documented examples](https://gitlab.com/jba-trust/ktp-ensemble/-/blob/master/nlp_flood/examples/README.md) 
of its use.

## Installation

This tool requires that you have [Python 3.7](https://www.python.org/downloads/release/python-377/#windows-users) installed. 
Unfortunately, certain necessary requisites of the tool require specifically **python verson 3.7**.

Download the zip file for [the latest release](https://gitlab.com/jba-trust/ktp-ensemble/-/archive/master/ktp-ensemble-master.zip) 
and extract it somewhere on your computer.

Navigate to the extracted folder and then into `nlp_flood/`.

To install the python environment for the tool, and install the necessary libraries, run the `install.bat` file, by 
double-clicking on it.

The tool also requires an API key for geocoding location data points. The tool expects a file called 
`api_key_geocoding_google.txt` to be in the `nlp_flood` folder, for this file to contain only the Google API key. 
For members of JBA, there is an API key provided in the Overview file in the `2018/KTP` WorkItems Folder.

## Usage: Data Extraction

If you are looking to extract the following types of flood data from reports, the tool may not need additional training.

### Flood Data

  * **Locations**: Places where floods may have taken place, including town names, road names, postcodes, that are 
    converted to latitude/longitude pairs, and full addresses where possible
  * **Causes**: Reasons for the flooding. For example, *'heavy rainfall'*.
  * **Sources**: The source of the flood water. For example, *'surface water'*.
  * **Datetimes**: The time and/or date of a flood occurence.
  * **Effects**: The effects the flood had, including damage to any buildings.
  * **Waterways**: The river, stream, channel, etc. involved in the flood, if any.

To run the tool on Windows, open a command line prompt and type the following command<sup>[1](#footnotes)</sup>:

```batch
env\Scripts\python.exe -u extract.py ^
 "C:\path\to\report" ^
 --label ^
 --output-type ^
 --output-path ^
 --country ^
 --provenance ^
 --quiet
```

**Arguments**:

  * `C:\path\to\model`: This is the filepath to the model used to extract the data.
    Unless you have trained your own model, the path to the model will be the `model` folder in this project.
  * `C:\path\to\reports`: These are the filepaths to the reports extract data from. You can include multiple filepaths.
  * `--output-path C:\path\to\output\file`: The path to the output file. The file will be created if it doesn't exist, 
    and will be overwritten if it does.
  * `--output_type csv`: You can choose the output file type. At the moment, only CSV is supported (see Output Types 
    below)
  * `--label LABEL_NAME`: You can specify to only extract certain flood data types by using this option (see Labels 
    below). You can include multiple labels. If no labels are included, then no data will be extracted.
  * `--country COUNTRY_CODE`: You can optionally provide a country code to use in the geocoding process. This should be 
    a two-character Country Code Top-Level Domain (ccTLD). For a list of ccTLDs, see 
    [this wikipedia section](https://en.wikipedia.org/wiki/List_of_Internet_top-level_domains#Country_code_top-level_domains).
  * `--provenance C:\path\to\output\file`: The path to the output file for any provenance data during extraction. The 
    file will be created if it doens't exist, and will be appended to if it does. Provenance data is a mapping of texts 
    in the reports that are further interpreted, like locations which are geocoded into latitudes and longitudes, to 
    their interpretations, so any anomalies, with geocoding servcies for example, can be found. If this argument is not 
    given then provenance data will not be saved.
  * `--quiet`: If this flag is included, then the program will only ouput errors.
  * `--debug`: If this flag is included, then the program will ouput debug information.
  * `--trace`: If this flag is included, then the program will ouput detailed execution information.

**Labels**:

The labels keywords are, for the default trained model, as follows: `LOCATION`, `CAUSE`, `SOURCE`, `DATETIME`, `EFFECT`,
and `WATERWAY`. If you have trained a new model on different labels, then use the names of those labels.

**Output Types**:

  * `csv`: The results are printed to a file in a [comma-separated format](https://www.howtogeek.com/348960/what-is-a-csv-file-and-how-do-i-open-it/).
  * `none`: No output file is created. This can be used by python scripts wanting to use the results directly.

**For Developers**:

As well as a standalone python script, the results can also be returned as a python array if the module is imported, as
shown below.

```python
import flood_extraction
analysis = flood_extraction.analyse(model_filepath, entities_to_extract, entity_formats, report_filepath)
results = analysis.output_results(flood_extraction.OutputType.NONE)
```

**Examples**:

There are [examples of text extraction](https://gitlab.com/jba-trust/ktp-ensemble/-/tree/master/nlp_flood%2Fexamples#text-extraction),
including flood data extraction, and the [work done for the RFO GACA](https://gitlab.com/jba-trust/ktp-ensemble/-/tree/master/nlp_flood%2Fexamples#advanced-text-extraction).

## Usage: Model Training

If you need a tool to extract different data, or would like it to extract data from different sources, you will need to
train the tool.

This requires data for both training and validation. These are the same, but are used for different purposes in the 
training process. To make this process easier, there is a script to generate empty training data from examples of the 
sort of files you'd like to extract data from.

The general rule of thumb is that 20% of the total data should be used for validation, with the 80% majority being used 
for training. 

It is important to be aware that any data points with the `LOCATION` type will currently use geocoding servcies to 
obtain a full address and latitude and longitude pair.  

### Creating Training Data

It is important to have training data that is a good representation of the type of data that the tool will be used to 
extract data from. If you expect a variety of sourcs, it is important to have all varieties represented.

To create a blank training data file from a file, use the following command<sup>[1](#footnotes)</sup>:

```batch
env\Scripts\python.exe -u training_creation.py ^
 "C:\path\to\report" ^
 "C:\path\to\report" ^
 "C:\path\to\report" ^
 --output "C:\path\to\output\directory" ^
 --format "blank_training_{}.txt" ^
 --plain ^
 --label LABEL_NAME_1 ^
 --label LABEL_NAME_2 ^
 --quiet
```

**Arguments**:

  * `C:\path\to\report`: These are the filepaths to the reports extract data from. You can include multiple filepaths.
  * `--output`: The path to the output file. The file will be created if it doesn't exist, and will be overwritten if it
    does.
  * `--format`: This is the format that any generated training files will use as their filename. The brackets `{}` will 
    be replaced with the report's filename.
  * `--plain`: This forces the program to treat any unknown filetypes as plaintext. If this is not included, then any 
    files with unrecognised filetypes will ignored.
  * `--label "LABEL_NAME"`: These are provided as prompts in the generated empty training file. You can include multiple
    labels, but these are not necessary for this to run.
  * `--quiet`: If this flag is included, then the program will only ouput errors.
  * `--debug`: If this flag is included, then the program will ouput debug information.
  * `--trace`: If this flag is included, then the program will ouput detailed execution information.

There are [examples](https://gitlab.com/jba-trust/ktp-ensemble/-/tree/master/nlp_flood%2Fexamples#training-data-creation)
of training data creation.

### Training the Tool

To use the training data you've created and train the tool on custom data types, use the following 
command<sup>[1](#footnotes)</sup>:

```batch
env\Scripts\python.exe -u train.py ^
 "C:\path\to\model" ^
 --training-data "C:\path\to\training\data" ^
 --training-data "C:\path\to\training\data" ^
 --test-data "C:\path\to\test\data*" ^
 --test-data "C:\path\to\test\data*" ^
 --label "LABEL_NAME_1" ^
 -i 100 ^
 --blank-model ^
 --rename "example_model" ^
 --quiet
```

**Arguments**:

  * `C:\path\to\your\model`: This is the filepath to the model.
  * `--blank-model`: If this flag is included, the training will start with a new blank model.
  * `--only-validate`: If this flag is included, no training will occur, but the training files will be checked and 
    validated.
  * `--input-dir C:\path\to\existing\model`: This is the path to an existing model. If one is not provided, it defaults 
    to the model filepath.
  * `--rename NEW_MODEL_NAME`: A new name for the mode.
  * `--iterations N`: The number of training iterations to perform.
  * `--training-data C:\path\to\training\data\file`: This is the filepath to training data. You can include multiple 
    filepaths.
  * `--test-data C:\path\to\test\data\file`: This is the filepath to testing data. You can include multiple 
    filepaths.
  * `--label LABEL_NAME`: You can specify to only train on certain labels. This defaults to all labels in the training 
    data. You can include multiple labels.
  * `--quiet`: If this flag is included, then the program will only ouput errors.
  * `--debug`: If this flag is included, then the program will ouput debug information.
  * `--trace`: If this flag is included, then the program will ouput detailed execution information.

There are [examples](https://gitlab.com/jba-trust/ktp-ensemble/-/tree/master/nlp_flood%2Fexamples#model-training)
of training both new and existing models.

## Footnotes

 1. All command line scripts assume that the user has navigated to the installed `ktp-ensemble` folder.
